
OPTION(FFMPEG_DLOPEN "Use FFMPEG by DLOPEN or as EXEC" ON)
OPTION(FFMPEG_EXEC "Use FFMPEG by DLOPEN or as EXEC" OFF)

OPTION(DEBUG "Enable verbose output." ON)
OPTION(DEBUG_ASSERTS "Enable asserts." ON)

OPTION(_GNU_SOURCE "Enable GNU source" ON)

SET(FFMPEG_SHLIB_PATH "/bla/bla")
SET(FFMPEG_BINARY_PATH "/bla/bla")

SET(CONF_INDEX_MAX_LENGTH 30)
SET(CONF_ALLOCATOR_PAGE_SIZE 512)

SET(ALLOCATOR_DEFAULT_PAGE_SIZE 4096)


INCLUDE(${CMAKE_CURRENT_LIST_DIR}/CMakeChecks.cmake)

IF(COMPILER_HAVE-fpic)
	ADD_COMPILE_OPTIONS(-fpic)
ENDIF()
IF(COMPILER_HAVE-O)
	ADD_COMPILE_OPTIONS(-O2)
ENDIF()
IF(COMPILER_HAVE-stdgnu11)
	ADD_COMPILE_OPTIONS(-std=gnu11)
ENDIF()
IF(COMPILER_HAVE-W)
	ADD_COMPILE_OPTIONS(-Wall -Wfatal-errors)
ENDIF()



CONFIGURE_FILE(config.h.in config.h @ONLY)
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

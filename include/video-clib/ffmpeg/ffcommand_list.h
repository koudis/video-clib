/**
 * In this library There are only global parameterized
 * arguments for ffmpeg like '-threads' and some
 * "allias" functions (as ffcommand_list_astring)
 *
 * This file is not full "option/value" list!
 *
*/



#ifndef VIDEO_CLIB_INCLUDE_FFMPEG_FFCOMMAND_LIST_H
#define VIDEO_CLIB_INCLUDE_FFMPEG_FFCOMMAND_LIST_H

#include <video-clib/ffmpeg/ffcommand.h>

/**
 * @brief Add "-threads <number>"
 *
 * Add "-threads <number>" option in the tail of <output> ffcommand section
 *
 * @param  command
 * @return void
 *
*/
void ffcommand_list_threads(struct ffcommand* command);



/**
 *  @brief Export command as string
 *
 *  This command is equivalent to
 *      ffcommand_lock(command);
 *      ffcommand_add_input(command, "-i", input_file, FFCOMMAND_TAIL);
 *      ffcommand_add_output(command, output_filename, NULL, FFCOMMAND_TAIL);
 *      ffcommand_export_string(command, mem, size);
 *      ffcommand_unlock(command, FFCOMMAND_HOLD);
 *  with handling return codes...
 *
 *  @param  commnad
 *  @param  input_file 
 *  @param  output_file
 *  @return int 0 - ok, >0 - really bad :)
*/
int ffcommand_list_astring(struct ffcommand* command, char* mem, size_t size,
		const char* const input_file, const char* const output_file);

/**
 * @brief Set Input and output files
 *
 * Just sets input nad output files.
 *
 * This function is equivalent of
 *     ffcommand_add_input(command, "-i", input_file, FFCOMMAND_TAIL);
 *     ffcommand_add_output(command, output_file, NULL, FFCOMMAND_TAIL);
 *
 * @param  command
 * @param  input_file
 * @param  output_file
 * @return int  - ok, >0 - :(
*/
int ffcommand_list_set_iofiles(struct ffcommand* commmand,
		const char* const input_file, const char* const output_file);



#endif

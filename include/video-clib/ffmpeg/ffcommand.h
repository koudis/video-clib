#ifndef VIDEO_CLIB_FFMPEG_COMMAND_H
#define VIDEO_CLIB_FFMPEG_COMMAND_H

#include <simclist.h>
#include <video-clib/assert.h>
#include <video-clib/mm/allocator.h>

#define FFORD_HEAD    1
#define FFORD_TAIL    2
#define FFORD_CURRENT 4
#define FFORD_MASK    (FFORD_HEAD | FFORD_TAIL | FFORD_CURRENT)

#define FFPAGE_HOLD   1
#define FFPAGE_DELETE 2
#define FFPAGE_MASK  (FFPAGE_HOLD | FFPAGE_DELETE)



/**
 * @brief List entry for 'list' in ffcommand_part
 * This struct is not public!
 * @see struct ffcommand_part
*/
struct ffcommand_list_entry {
	char*             value;       /**< Own value of list */
	struct ffcommand* command;     /**< Back reference to the parent */
	int               order;       /**< Order */
	int               real_order;
};

/**
 * @brief Part of ffmpeg command...
 * @see ffcommand
 * @see ffcommand_list_entry
*/
struct ffcommand_part {
	int    persist;       /**< @see ffcommand_lock. Holds the list size... */
	int    max;           /**< MAX value of order(sort) attribute. Default val. = 0 */
	int    min;           /**< MIN value of order(sort) attribute. Default val. = 0 */
	list_t list;          /**< */
};

/**
 * @brief linked list varian of ffmpeg command
 *
 * that structore represent ffmpeg command.
 *   ffmpeg [input options] -i [input filename] -codec:v [video options]
 *     -codec:a [audio options] [output file options] [output filename]
 *
 *   ffmpeg <<input>> <video> <audio> <<output>> where
 *   -# input    = <input> -i <input_file>
 *   -# video    = -codec:v [video options] // ??
 *   -# audio    = -codec:a [audio options] // ??
 *   -# output   = <output> <output_file>
 *
*/
struct ffcommand {
	struct allocator*       allocator;
	struct ffcommand_part*  parts[4];
	struct allocator_state  al_state;
	int lock;
	int order_priority; /**< 0 - order by 'real_order', >0 - order by 'order' */
};




/**
 * Init ffcommand structure
 * @param command
 * @return int 0 - ok, >0 error occured
*/ 
int ffcommand_init(struct ffcommand* command);

/**
 * Destroy allocated memory in command...
 * @param command
 * @return void
*/
void ffcommand_destroy(struct ffcommand* command);



/**
 * Get number of elements in command list
 *
 * @param  command
 * @return size_t
*/
static inline unsigned int ffcommand_size(struct ffcommand* command) {
	unsigned int size = 0;
	for(int i = 0; i < 4; i++)
		size += list_size(&command->parts[i]->list);
	return size;
}



/**
 * @brief Lock the command
 *
 * - Create a new allocator page
 * - Lock allocator
 *
 * All changes made by ffmpeg_add functions will be stored
 * in new the new alloc. page.
 *
 * @param command
 * @return 0 - locked sucessful, 1 - command was already locked
*/
int ffcommand_lock(struct ffcommand* command);

/**
 * @brief Unlock command
 *
 * Trash out all changes commited after the ffcommand_lock call.
 * If there is no ffcommand_lock before this function call, then
 * error occurs.
 *
 * This function just fetch out all changes, unlock allocator and free current page 
 *
 * @param command
 * @param hold Hold 'lock' memory? (true - Yes, false - no)
 * @return 0 - ok, >0 - :(
 *
*/
int ffcommand_unlock(struct ffcommand* command, int hold);



/**
 * Export command into string in form
 *     ffmpeg <input> <video> <audio> <output>
 *
 * This function must by call inside the
 *  ffcommad_lock(...)
 *    <export functions>
 * 	ffcommand_unlock(...)
 *
 * @param command
 * @param al allocated memory for export program...
 * @param size number of bytes in al (size of string)
 * @return int number of used bytes in al. Return -1 if fail.
*/
int ffcommand_export_string(struct ffcommand* command, char* al, size_t size);

/**
 * Export linked-list into array. Items in array are references to the
 * values in linked-list (no copy)!
 *
 * This function must by call inside the
 *  ffcommad_lock(...)
 *    <export functions>
 * 	ffcommand_unlock(...)
 *
 * @param command
 * @param al   Allocated memory for export program...
 * @param size Max number of items...
 * @return int Number of used bytes in al. Return -1 if fail
*/
int ffcommand_export_array(struct ffcommand* command, char** al, size_t size);



/**
 * Add specified opt into video section
 *
 * @param command
 * @param opt
 * @param val If null, then opt have no value segment
 * return int 0 = ok, >0= not fine
*/
int ffcommand_add_video(struct ffcommand* command,
		const char* const opt, const char* const val, int fforder);

/**
 * Add specified opt into audio section
 * @param command
 * @param opt
 * @param val If null, then opt have no value segment
 * return int 0 = ok, >0= not fine
*/
int ffcommand_add_audio(struct ffcommand* command,
		const char* const opt, const char* const val,  int fforder);

/**
 * Add specified opt into input section
 *
 * @param command
 * @param opt
 * @param val If null, then opt have no value segment
 * return int 0 = ok, >0= not fine
*/
int ffcommand_add_input(struct ffcommand* command,
		const char* const opt, const char* const val, int fforder);

/**
 * Add specified opt into output section
 *
 * @param command
 * @param opt
 * @param val If null, then opt have no value segment
 * return int 0 = ok, >0= not fine
*/
int ffcommand_add_output(struct ffcommand* command,
		const char* const opt, const char* const val, int fforder);



#endif

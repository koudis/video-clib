/**
 * Error handling.
 *
 * For more details see linux_repo:tools/include/linux/err.h
 *
 * This file use uintptr_t and intptr_t which is not guaranteed by
 * C standard. 
 *
*/

#ifndef VIDEO_CLIB_ERROR_H
#define VIDEO_CLIB_ERROR_H

#include <video-clib/macro.h>

#include <stdarg.h>
#include <stdint.h>

#define MAX_ERRNO 255

#define IS_ERR_VALUE(x) unlikely((x) >= (uintptr_t)-MAX_ERRNO)

/**
 * @brief Return codes...
 *
 * This codes is used for propagation errors to parents
*/
enum error {
	E_ERROR = 1, // Error, but system can recover...
	E_CRIT       // Error, but system cannot recover...
};



static inline void*
ERR_PTR(uintptr_t error_) {
	return (void*) error_;
}

static inline intptr_t
PTR_ERR(const void *ptr) {
	return (intptr_t) ptr;
}

static inline int
IS_ERR(const void *ptr) {
	return IS_ERR_VALUE((uintptr_t)ptr);
}

/**
 * @brief Die after unrecoverable error
 *
 * log and die :)
*/
void
_die(const char* msg, ...) NO_RETURN FORMAT_CHECK(printf, 1, 2);

#ifndef DEBUG
#define die(M, ...) _die (M, ##__VA_ARGS__)
#else
#	define die(M, ...) _die("%s - "#M, __FUNCTION__, ##__VA_ARGS__)
#endif


#endif

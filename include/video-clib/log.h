/**
 * ;
 *
 *
*/



#ifndef VIDEO_CLIB_LOG_H
#define VIDEO_CLIB_LOG_H

#include <stdio.h>
#include <video-clib/macro.h>
#include <video-clib/error.h>

/**
 * L - level, M - message, R - return code
*/
#define LOG_AND_RETURN(L, R, M, ...) ({par_log(L, M "\n", ##__VA_ARGS__); return R;} )

#define LOG_AND_PRETURN(L, R, M, ...) ({par_log(L, M "\n", ##__VA_ARGS__); return ERR_PTR(-R);} )



/**
 * @brief Log levels copied from syslog
*/
enum log_level {
	L_EMERG = 0,  //system is unusable
	L_ALERT,      //action must be taken immediately
	L_CRIT,       //critical conditions
	L_ERROR,      //error conditions
	L_WARNING,    //warning conditions
	L_NOTICE,     //normal, but significant, condition
	L_INFO,       //informational message
	L_DEBUG       //just a debug msg
};

/**
 * @brief global struct holding attribs for log
 *
 * Data in this struct MUST BE thread safe...
*/
struct log_struct {
	FILE* descriptor; /**< Default is stderr */ 
	int   visible;    /**< boolean flag. If true and stderr is not closed
						then is used as log output together with file_descriptor */
};

#ifndef DEBUG
#	define LOG_VISIBLE_DEFAULT 0
#else
#	define LOG_VISIBLE_DEFAULT 1
#endif



void par_log_init() CONSTRUCTOR;

/**
 * Just log a message
 *
 * @param l   Log level...
 * @param msg Log message
 *
*/
int
par_log(enum log_level l, const char* msg, ...) FORMAT_CHECK(printf, 2, 3);

/**
 * Same as log, but suppert va_list as third argument
 *
*/
int
par_vlog(enum log_level l, const char* msg, va_list va_l);



#endif

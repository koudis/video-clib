#ifndef VIDEO_CLIB_FFMPEG_H
#define VIDEO_CLIB_FFMPEG_H

#include <video-clib/ffmpeg/ffcommand.h>



enum ffmpeg_eror {
	EF_ERROR = E_ERROR,
	EF_CRIT  = E_CRIT,
	EF_CECA,   // Cannot Export Command as Array
};



struct ffmpeg_struct {
	struct ffcommand* command;
	char** mem;
	size_t mem_size;
};



/**
 * @brief FFmpeg main function (@see FFmpeg/ffmpeg.c::main)
 *
 * Open the FFMPEG_SHLIB_PATH library and run  'main' function
 * If there is no ffmpeg shared library, or dlopen cannot open library or
 * there is no main function in the shared library then die occured.
 *
 * @param  argc
 * @param  argv
 * @return int Return code of ffmpeg 'main' function
*/
int
ffmpeg(int argc, char** argv);

/**
 * @brief Same as ffmpeg
 * @see ffmpeg
 *
*/
int
ffmpegl(struct ffmpeg_struct* ffs);

/**
 * @brief Prepare ffmpeg_struct for ffmpegl
 *
 * Export command into mem by ffcommand_export_array function.
 *
 * @param  ffs
 * @return int
*/
int
ffmpegl_prepare(struct ffmpeg_struct* ffs);




#endif

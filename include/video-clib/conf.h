/**
 *
 * Converts YAML file into Judy SL's arrays...
 *
*/



#ifndef INCLUDE_CONF_H
#define INCLUDE_CONF_H

#include <video-clib/error.h>
#include <video-clib/macro.h>
#include <video-clib/mm/allocator.h>



enum conf_error {	
	EC_ERROR = E_ERROR,
	EC_CRIT  = E_CRIT,
	EC_ENOENT,   // Cannot open the file
	EC_YPE,      // Cannot init yaml parser
	EC_IFRM,     // Invalid format
	EC_UNSUP     // Unsupported token
};


/**
 *
*/
enum conf_type {
	CONF_SCALAR = 1,
	CONF_BLOCK_MAPPING,
	CONF_BLOCK_SEQUENCE,
};



/**
 * Hold information about loaded conf
*/
struct conf {
	void*  judy;
	struct allocator* allocator;
};

/**
 * Conf entry.
 * Content of each entry in Judy array
 *
*/
struct conf_value {
	void* value;
	enum conf_type type;
};



/**
 * Init conf
 * @param conf
*/
int
conf_init(struct conf* conf);

/**
 * Destroy given conf
 * @param void* Judy array
 * @return boolean
*/
void
conf_destroy(struct conf* conf);



/**
 * Load conf. from configuration file
 * @param  conf
 * @param  file
 * @return boolean
*/
int
conf_loadf(struct conf* conf, const char* const file);

/**
 * Load conf. from stream fh
 * @param conf
 * @param fh
 * return boolean
*/
int
conf_load(struct conf* conf, FILE* fh);



/**
 * example.yaml:
 *     one:
 *         - one
 *         - two
 *         - three: [1, string, 3]
 * usage:
 * struct conf conf;
 * [conf init]
 * void* val     = conf_get(&conf, "one", "2", "three");
 * void* three   = conf_get(val, "0")
 * void* string  = conf_get(val, "1")
 * void* string2 = conf_get(&conf, "one", "2", "three", "1");
 * puts((char*)three);    // prints "1\n"
 * puts((char*)string);   // prints "string\n"
 * puts((char*)string2);  // prints "string\n"
 *
 * @see video-clib/macro.h::VA_NARGS
 * @see conf_getc
 * @see conf_getj
 *
 * @param conf
 * @param list of config keys
 * @return void*
 * 
*/
#define conf_get(conf_arg, args...) _Generic((conf_arg), \
			struct conf*: conf_getc(conf_arg, VA_NARGS(args), args), \
			void*: conf_getj(conf_arg, VA_NARGS(args), args) \
		)

/**
 * Return values under keys '...'
 * If there is no such keys, return NULL.
 * template:
 * 		my_key1:
 * 			my_key2: value
 * usage:
 * 		value = conf_getc(conf, "my_key1", "my_key2");
 *
 * @param conf
 * @param num
 * @return void* Valid pointer or NULL on failure
 *
*/
void*
conf_getc(struct conf* conf, unsigned int num, ...);

/**
 * @see conf_getc
*/
void*
conf_getj(void* judy, unsigned int num, ...);




#endif

/**
 * Allocator library for allocate and manage large
 * of memory chunks.
 *
 * Each memory chunk allocated by allocator_malloc
 * is aligned at multiple of (__WORDSIZE/8)
 *
*/



#ifndef VIDEO_CLIB_INCLUDE_ALLOCATOR_H
#define VIDEO_CLIB_INCLUDE_ALLOCATOR_H

#include <stdint.h>
#include <linux/types.h>
#include <video-clib/error.h>
#include <simclist.h>

#define ALLOCATOR_PAGE_START(x) ((uint8_t*)(x)->mem - (x)->used)
#define ALLOCATOR_PAGE_END(x)   ((uint8_t*)(x)->mem + (x)->size)
#define ALLOCATOR_PAGE_CUR(x)   ((uint8_t*)(x)->mem + (x)->used)
#define ALLOCATOR_PAGE_MEM(x)   ((uint8_t*)(x)->mem)

#define ALLOCATOR_STATE_VALID(st) ((st)->page_number >= 0 && (st)->size > 0)

#define ALLOCATOR_IS_PAGE_CLEAN(x) (!((x)->used))

#define ALLOCATOR_PAGE_STATIC_INIT(_size) ((struct allocator_page){\
		.size = _size, .used = _size, .page_number = -1, .mem = NULL})



enum allocator_err {
	EA_ERROR = E_ERROR,
	EA_CRIT  = E_CRIT
};



/**
 * @brief info about actual page entry
 *
 * memory is allocated in pages:
 *  -# Each page is entry of linked list
 *  -# Each page has "size" allocated bytes
 *  -# "used" opt indicates used bytes of "size" allocated bytes
 *  
 *
 * @see struct allocator
*/
struct allocator_page {
	size_t size;
	size_t used;        /**< number of used memory bytes */
	int    page_number; /**< page number (in {0, 1...}) */
	void*  mem;         /**< reference to the memory page */
};

#define allocator_state allocator_page

/**
 * @brief list of memory page entries
 *
 * -# Each data entry in the "list" is allocatad memory page.
 * -# End of this list is last allocated partly-free memory page
 * -# For any memory page which is not in the end of that "list":
 *      Page is fully "allocated/used" and cannot be used
 *      inside allocator_malloc
*/
struct allocator {
	int lock;                       /**< Just don't create another page, if lock is true */
	list_t* list;                   /**< list of memory pages */
	struct allocator_page* page;    /**< current list page entry */
};



/**
 * inicialize allocator struct :).
 * @param al        allocator to be inicialized
 * @param page_size page_size (if zero, getpagesize() will be used)
 * @return int 0 - success, >0 - :(
*/
int allocator_init(struct allocator*, const size_t page_size);

/**
 * Destroy allocator
 * @param al reference to allocator
 * @return int =0 - success, !=0 - :(
*/
int allocator_destroy(struct allocator* al);



/**
 * @brief Variant of malloc function.
 *
 * Works on allocator defined/allocated memory pages...
 * If there is not enought space in current page, it will allocated
 * a new page, push it inside the "list" and set it as current page 
 * and increment page_number.
 * Then return a pointer (from a new page)
 * 
 * @pram  al   alocator instance
 * @param size size of bytes (as malloc)
 * @return reference to the allocated memory
 *
*/
void* allocator_malloc(struct allocator* al, const size_t size);



/**
 * @brief Save the current allocator state into the 'st' (page size, used memory and mem ref)
 * @see allocator_create_newpage
 * @see allocator_delete_lastpage
 *
 * This construct allow create clever temporary pages
 * by allocator_create_newpage (and delete by allocator_delete_lastpage)
 *
 * This function can by called several times in succession. (but with different 'st')
 *
 * Do not use state_save and state_retore without allocator_create_newpage...
 *
 * Use:
 *   allocator_state_save(state);
 *   allocator_create_newpage(...);
 *   allocator
 *   <doo>
 *
 * @param al
 * @param st
 * @return 0 - ok, >0 bug occured
*/
int allocator_state_save(struct allocator* al, struct allocator_state* st);

/**
 * @brief Restore allocator state saved in 'st'
 *
 * @param al
 * @param st
 * @return 0 - ok, >0 bug occured
*/
int allocator_state_restore(struct allocator* al,  struct allocator_state* st);



/**
 * @brief Prevent create a new page
 *
 * If current page is locked, functions
 *   allocator_create_newpage(...)
 *   allocator_delete_lastpage(...)
 * return false 
 *
 * @param al
 * @return int 0 - success, >0 - :(
*/
int allocator_page_lock(struct allocator* al);

/**
 * @brief Unlock current page
 *
 * @param al
 * @return int 0 - success, >0 - :(
*/
int allocator_page_unlock(struct allocator* al);



/**
 * @brief Clean current page :)
 *
 * Just invalidate all pointers in current page
 *
 * @param al
 * @return 0 - success, >0 - :(
 *
*/
int allocator_cleanpage(struct allocator* al);

/**
 * @brief Free all memory in current page and fetch the last but one mem page
 *
 * Free all memory in current page and fetch the last but one mem page.
 * If there is no 'second' page, then...
 *
 * This function is useful for creating (and "deleting") temporary object...
 * 
 * @param al
 * @return 0 - success, >0 - :(
*/
int allocator_delete_lastpage(struct allocator* al);

/**
 * @brief Create a new page 
 *
 * Create a new page. If there is a current clean page
 * then no page will be created and current page is used instead.
 *
 * This function is useful for creating temporary objects...
 *
 * Use (with save_state):
 *   allocator_state_save(al, st);
 *   allocator_create_newpage(al);
 *   <do something>
 *   allocator_create_newpage(al);
 *   <do something>
 *   allocator_delete_lastpage(al); // we must call allocator_delete twice...
 *   allocator_delete_lastpage(al);
 *   allocator_state_restore(al, st);
 *   // Now, we are in state 'st'
 *
 * @param al
 * @return 0 - success, >0 - :(
*/
int allocator_create_newpage(struct allocator* al);



#endif

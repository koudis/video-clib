#ifndef VIDEO_CLIB_INCLUDE_MM_MMAP_H
#define VIDEO_CLIB_INCLUDE_MM_MMAP_H

#include <video-clib/error.h>



enum mm_mmap {
	MM_MERROR = E_ERROR,
	MM_MCRIT  = E_CRIT,
	MM_MCOPS,   // Cannot open file
	MM_MTRNC,   // Cannot truncate file to the specified size
	MM_MSFAI,   // Cannot sync
	MM_MMFAI    // Cannot map the file
};



/**
 * @brief Map 'name' file with size 'length' into memory.
 *
 * @param name   Name of virtual(/tmp) mapped file
 * @param length See man mmap(2)
 * @param prot   See man mmap(2)
 * @param flags  See man mmap(2)
 * @return void*
*/ 
void* par_mmap(const char* const name, size_t length, int prot, int flags);



#endif

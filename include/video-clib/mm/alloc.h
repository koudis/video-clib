

#ifndef VIDEO_CLIB_MM_ALLOC_H
#define VIDEO_CLIB_MM_ALLOC_H

#define xmalloc par_malloc
#define xrealloc par_realloc
#define xfree   par_free

#include <stdlib.h>

void* par_malloc(size_t size);

void* par_realloc(void* ptr, size_t size);

void par_free(void* p);


#endif

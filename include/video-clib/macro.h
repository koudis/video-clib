/**
 *
 *
 *
*/

#ifndef VIDEO_CLIB_INCLUDE_MACRO_H
#define VIDEO_CLIB_INCLUDE_MACRO_H

#include <stdlib.h>
#include <stdio.h>



#define EXIT(RCODE, M, ...) { exit(RCODE);}

//#define ENUM_ITEM(name, order)             name = ((int)((~0) ^ (1 << order)))
//#define ENUM_MASK(name, orders)            name = ((int)((int)(~0) >> (8*sizeof(int) - orders)))
//#define ENUM_MASK_SUPPLEMENT(name, orders) name = (~ENUM_MASK(orders))
#define ENUM_UNIQ(order, mask) ((((order - 1) & order) == 0) && (order <= mask))

/* Count number of arguments passed to a variadic macro  */
#define VA_NARGS_IMPL(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, N, ...) N
#define VA_NARGS(...) VA_NARGS_IMPL(__VA_ARGS__, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)


/**
 * See commit 8c87df4 in Linux Kernel
 * (maybe use COMPILE_ASSERT instead of ...)
*/
#define BUILD_BUG_ON_ZERO(e) (sizeof(struct { int:-!!(e); }))
#define BUILD_BUG_ON_NULL(e) ((void *)sizeof(struct { int:-!!(e); }))

/**
 * Lets use a small user optimalization
*/
#define likely(x)   __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)


// Check printf-like format...
#define FORMAT_CHECK(x,y,z) __attribute__((format(x,y,z)))

#define NO_RETURN __attribute__((noreturn))

#define ALIAS(x) __attribute__((alias(#x)))

#define WEAK __attribute__((weak))

#define CONSTRUCTOR __attribute__((constructor))



#endif

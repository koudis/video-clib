/**
 * Run program as a child... 
 *
*/

#ifndef VIDEO_CLIB_PARALLEL_H
#define VIDEO_CLIB_PARALLEL_H

#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <video-clib/mm/allocator.h>
#include <video-clib/error.h>




/**
 *
*/
enum parallel_error {
	EP_ERROR  = E_ERROR,	// The maximum number of running process reached...
	EP_CRIT   = E_CRIT,     // Critic error :(
	EP_CCLN,                // Cannot clone
	EP_UCLNF,               // Unsupported clone flags
	EP_NEMEM,               // Not enaught "space"...
	EP_CCWT                 // Cannot create waiting thread
};

/**
 * @brief Function passed to the clone...
*/
typedef int (*parallel_main)(void* a);

/**
 * @brief User defined function for handling 
 * User must handle waitpid status and return code!
 *
 * @param rsp   report after "main" return (see on @parallel_job)
 * @param udata user 
 * @return int >0 - just try wait again. 0 - free stack
*/
struct parallel_report;
struct parallel_job;
typedef int (*parallel_response)(struct parallel_report* rsp, struct parallel_job* job);

/**
 * @brief Report information after child return
 *
 * Structure contains values described by wait and waitpid functions.
 * For more information see "man 2 2 wait"
 *
*/
struct parallel_report  {
	int   status;        /**< Status code of waitpid function */
	int   verrno;        /**< Copy of errno if error occurs */
	pid_t wpid;          /**< waitpid return value */
};

/**
 * @brief Job for process
 *
*/
struct parallel_job {
	parallel_main     main; /**< "fn" for clone (see "man clone") */
	parallel_response rsp;  /**< When "main" returns this function is run */
	void* udata;            /**< Reference to the user data space. @see parallel_response, @see parallel_wait */
	void* stack;
	int   pid;              /**< Process Id of running job */
};

/**
 * @brief
 *
*/
struct parallel_wait {
	pthread_t*       thread;
	pthread_attr_t*  attr;
	sem_t*           pick;    /**< Count terminated but not "picked" child processes */
	int              run;     /**< is there a waiting thread? */
};

/**
 * @brief Structure for handle parallel requests
 *
*/
struct parallel {
	struct allocator* allocator;  /**< Memory pool for udata in @parallel_job and whoole parallel_job struct and stack */
	struct parallel_job** jobs;   /**< List of job descriptors */
	struct parallel_wait* wait;   /**< Structure needed for wait thread */
	struct {   // let's hold info about stacks... 
		size_t  size;             /**< the stack size */
		void**  inuse;            /**< Used stacks */
		void**  free;             /**< Unused stacks (unused stacks) */
	} stack;
	pthread_mutex_t* mutex;       /**< Mutex for synchronize access to parallel struct */
	sem_t*           sem;         /**< Count running processes  */
	int              max;         /**< Max of running jobs. This is a constant! */
};



/**
 * @brief Init parallel struct...
 *
 * @param p   parallel struct for init
 * @param max maximum of running childs managed by parallel
 *
*/
int
parallel_init(struct parallel* p, uint8_t max);



/**
 * @brief Stop all running proceses and destroy...
 *
 * -# wait on all running childs
 * -# for each stoped child execute parallel_report function
 *    (rsp in parallel_job)
 * -# destroy parallel struct
 *
 * @param p
 * @return void
*/
void
parallel_destroy(struct parallel* p);



/**
 * @brief Just start "p"
 *
 * @param  p      parallel...
 * @param  job    just o new job for run :)
 * @param  cflags Permit all clone flags except CLONE_THREAD
 * @param  a      Reference to the argument of parallel_main 
 * @param noblock 
 * @return Return 0 if all ok, else return 1
*/
int
parallel_start(struct parallel* p, struct parallel_job* job, int cflags, void* a, int noblock);



/**
 * @brief Wait on child processes.
 *
 * - In first call, start thread for waiting on children
 *   clonned by parallel_start...
 * - If child clonned by parallel_start returns (or terminate), than 
 *     -# parallel_report is created,
 *     -# parallel_response is run with the parallel_report instance
 *        and the udata. Where udata is user space allocated memory...
 * - If there is no children for wait, then thread is destroyed. 
 *   (and can be created again by parallel_wait) and zero is returned
 *
 * - If block is true, function block the caller until all processes
 *   started by parallel_start ended
 *
 * - If there is no process for wait, zero is returned
 *
 *
 * @param p struct parallel
 * @param block
 * @return int
*/
int
parallel_wait(struct parallel* p, int block);



#endif

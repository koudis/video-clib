/**
 *
 * Assert replacement for assert macro in <assert.h>
 *
 *
*/

#ifndef VIDEO_CLIB_ASSERT_H
#define VIDEO_CLIB_ASSERT_H

#include <config.h>
#include <video-clib/macro.h>

#ifndef DEBUG_ASSERTS
#	define ASSERT_WRITEABLE(ptr) do { } while(0)
#	define ASSERT(COND, ...) do { } while(0)
#else
#	define ASSERT_WRITEABLE(ptr) ({ volatile char *__p = (ptr); *__p = *__p; })
#	define ASSERT(COND) ({ if(likely(!(COND))) _assert_failed(#COND, __LINE__, __FILE__); })
#endif

#define COMPILE_ASSERT(name, x) typedef char COMPILE_ASSERT_##name[!!(x)-1]


/**
 *
 * @param cond Conditiond which invokes _assert
 * @param line Line niumber
 * @param file File name
*/
void
_assert_failed(const char* cond, int line, const char* file) WEAK;



#endif


#include <video-clib/conf.h>
#include <Judy.h>

#include <stddef.h>
#include <setjmp.h>
#include <stdlib.h>
#include <string.h>
#include <cmocka.h>

const char* log_context_name = "CONF";



static void
_check_bad_file_name(void** state) {
	struct conf** conf = (struct conf**)*state;

	int _tmp = conf_loadf(conf[0], "files/conf/iam_a_black_rabbit");
	assert_int_equal(_tmp, EC_ENOENT);
}

static void
_check_bad_flow_sequence(void** state) {
	struct conf** conf = (struct conf**)*state;

	int _tmp = conf_loadf(conf[0], "files/conf/bad_flow_sequence.yaml");
	assert_int_equal(_tmp, EC_IFRM);
}

static void
_check_bad_flow_mapping(void** state) {
	struct conf** conf = (struct conf**)*state;

	int _tmp = conf_loadf(conf[0], "files/conf/bad_flow_mapping.yaml");
	assert_int_equal(_tmp, EC_IFRM);
}

static void
_check_bad_block_sequence(void** state) {
	struct conf** conf = (struct conf**)*state;

	int _tmp = conf_loadf(conf[0], "files/conf/bad_block_sequence.yaml");
	assert_int_equal(_tmp, EC_IFRM);
}

static void
_check_bad_block_mapping(void** state) {
	struct conf** conf = (struct conf**)*state;

	int _tmp = conf_loadf(conf[0], "files/conf/bad_block_mapping.yaml");
	assert_int_equal(_tmp, EC_IFRM);
}



static int
compare_two_conf_struct(void* judy1, void* judy2);

static void
_check_eq_arrays(void** state) {
	struct conf** conf = (struct conf**)*state;

	int _tmp = conf_loadf(conf[0], "files/conf/eq_1.yaml");
	assert_true(!_tmp);

	_tmp = conf_loadf(conf[1], "files/conf/eq_2.yaml");
	assert_true(!_tmp);

	assert_true(!compare_two_conf_struct(conf[0]->judy, conf[1]->judy));
}



int
startup(void** state) {
	struct conf** c = malloc(2*sizeof(struct conf*));
	c[0] = malloc(sizeof(struct conf));
	c[1] = malloc(sizeof(struct conf));
	if(!c || !c[0] || !c[1])
		return 1;
	if(conf_init(c[0]) || conf_init(c[1]))
		return 2;

	*state = c;
	return 0;
}

int
teardown(void** state) {
	struct conf** c = *state;

	conf_destroy(c[0]);
	conf_destroy(c[1]);
	free(c[0]); free(c[1]); free(c);
	return 0;
}

int
teardown_failedinit(void** state) {
	struct conf** c = *state;

	conf_destroy(c[1]);
	free(c[0]); free(c[1]); free(c);
	return 0;
}

#define m_add_unit(func) cmocka_unit_test_setup_teardown(func, startup, teardown)
#define m_add_unitfi(func) cmocka_unit_test_setup_teardown(func, startup, teardown_failedinit)

int main() {
	const struct CMUnitTest tests[] = {
		m_add_unitfi(_check_bad_file_name),
		m_add_unitfi(_check_bad_flow_sequence),
		m_add_unitfi(_check_bad_flow_mapping),
		m_add_unitfi(_check_bad_block_sequence),
		m_add_unitfi(_check_bad_block_mapping),
		m_add_unit(_check_eq_arrays),
	};

	return cmocka_run_group_tests(tests, NULL, NULL);
}



static int
compare_two_conf_struct(void* judy1, void* judy2) {
	uint8_t index1[200], index2[200];
	PWord_t value1, value2;
	struct conf_value *conf_value1, *conf_value2;

	index1[0] = '\0'; index2[0] = '\0';
    JSLF(value1, judy1, index1);
    JSLF(value2, judy2, index2);

	int _tmp = 0;
	while (value1 != NULL) {
		if(value2 == NULL)
			return 1;
		if(strcmp((char*)index1, (char*)index2))
			return 1;

		conf_value1 = (struct conf_value*)(*value1);
		conf_value2 = (struct conf_value*)(*value2);

		if(conf_value1->type == CONF_BLOCK_MAPPING) {
			if(conf_value2->type != CONF_BLOCK_MAPPING)
				return 1;
			 _tmp = compare_two_conf_struct(conf_value1->value, conf_value2->value);
			 if(_tmp)
				 return 1;
		}

		if(conf_value1->type == CONF_BLOCK_SEQUENCE) {
			if(conf_value2->type != CONF_BLOCK_SEQUENCE)
				return 1;
			_tmp = compare_two_conf_struct(conf_value1->value, conf_value2->value);
			if(_tmp)
				return 1;
		}

		if(conf_value1->type == CONF_SCALAR &&
				conf_value2->type == CONF_SCALAR) {
			_tmp = strcmp(conf_value1->value, conf_value2->value);
			if(_tmp)
				return _tmp;
		}

		JSLN(value1, judy1, index1);
		JSLN(value2, judy2, index2);
	}

	return 0;
}

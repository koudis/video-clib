
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <cmocka.h>

#include <video-clib/ffmpeg/ffcommand.h>
#include <video-clib/macro.h>

const char* log_context_name = "COMMAND";



#define m_check_init int _s

static int EXPECT_ASSERT_LO = 0;
#define m_check_expect_assert(yes) \
	if(yes) \
		EXPECT_ASSERT_LO = 1; \
	else \
		EXPECT_ASSERT_LO = 0;

#define m_check_val_null(sec, command, opt, ORDER) \
	if(EXPECT_ASSERT_LO) { \
		expect_assert_failure(ffcommand_add_##sec(command, #opt, NULL, ORDER)); \
	} else { \
		_s = ffcommand_add_##sec(command, #opt, NULL, ORDER); \
		assert_int_equal(_s, 0); \
	}

#define m_check_opt_val_null(sec, command, ORDER) \
	expect_assert_failure(ffcommand_add_##sec(command, NULL, NULL, ORDER));

#define m_check_ov_with_order(command, ORDER) \
	m_check_val_null(input,  command, "1", ORDER);\
	m_check_val_null(video,  command, "2", ORDER);\
	m_check_val_null(audio,  command, "3", ORDER);\
	m_check_val_null(output, command, "4", ORDER);\
	m_check_opt_val_null(input,  command, ORDER);\
	m_check_opt_val_null(video,  command, ORDER);\
	m_check_opt_val_null(audio,  command, ORDER);\
	m_check_opt_val_null(output, command, ORDER)

static void _check_add_functions(void** state) {
	struct ffcommand* command = *state;
	m_check_init;

	m_check_expect_assert(0);
	m_check_ov_with_order(command, FFORD_TAIL);
	m_check_ov_with_order(command, FFORD_HEAD);
	m_check_ov_with_order(command, FFORD_CURRENT);

	// Unsupported flags
	m_check_expect_assert(1);
	m_check_ov_with_order(command, ~FFORD_MASK);
	m_check_ov_with_order(command, FFORD_HEAD | FFORD_TAIL | ~FFORD_MASK);
	m_check_ov_with_order(command, FFORD_HEAD | FFORD_TAIL);
	m_check_ov_with_order(command, FFORD_HEAD | FFORD_TAIL | FFORD_CURRENT);
	m_check_ov_with_order(command, FFORD_HEAD | FFORD_CURRENT);
}

static void _check_empty_command(void** state) {
	struct ffcommand* command = *state;
	char mem[10]      = { '\0' } ;
	char* mem_ref[10] = { 0 };

	int _s = ffcommand_export_string(command, mem, 10);
	assert_int_equal(_s, 0);
	assert_string_equal(mem, "");

	_s = ffcommand_export_array(command, mem_ref, 10);
	assert_int_equal(_s, 0);

	ffcommand_lock(command);
	_s = ffcommand_export_string(command, mem, 10);
	assert_int_equal(_s, 0);
	assert_string_equal(mem, "");

	_s = ffcommand_export_array(command, mem_ref, 10);
	assert_int_equal(_s, 0);
}

static void _check_export(void** state) {
	struct ffcommand* command = *state;
	char  mem[100]     = "MEM";
	char* mem_ref[100] = { 0 };
	int _s;

	ffcommand_add_audio(command, "1", NULL, FFORD_TAIL);

	// is there a check for enough space for empty string?
	_s = ffcommand_export_string(command, mem, 0);
	assert_int_equal(_s, -1);
	_s = ffcommand_export_array(command, mem_ref, 0);
	assert_int_equal(_s, -1);

	// check number of written bytes
	// " " + "1" = 2
	_s = ffcommand_export_string(command, mem, 100);
	assert_int_equal(_s, 2);
	assert_string_equal(mem, " 1");

	_s = ffcommand_export_array(command, mem_ref, 100);
	assert_int_equal(_s, 1);
	assert_string_equal(mem_ref[0], "1");


	// Just add some values into video section...
	ffcommand_add_video(command, "M", "2-2", FFORD_TAIL);

	// " M 2-2" + " 1" = 8
	_s = ffcommand_export_string(command, mem, 100);
	//assert_int_equal(_s, 8);
	assert_string_equal(mem, " M 2-2 1");

	// OUTPUT
	ffcommand_add_output(command, "-o", "file", FFORD_TAIL);

	// " M 2-2" + " 1" + " -o" + " file" = 16
	_s = ffcommand_export_string(command, mem, 100);
	assert_int_equal(_s, 16);
	assert_string_equal(mem, " M 2-2 1 -o file");


	// Just add some values into video section...
	ffcommand_add_video(command, "BEGIN", NULL, FFORD_HEAD);

	_s = ffcommand_export_string(command, mem, 100);
	assert_int_equal(_s, 22);
	assert_string_equal(mem, " BEGIN M 2-2 1 -o file");

	_s = ffcommand_export_array(command, mem_ref, 100);
	assert_int_equal(_s, 6);
	assert_string_equal(mem_ref[0], "BEGIN");
	assert_string_equal(mem_ref[1], "M");
	assert_string_equal(mem_ref[2], "2-2");
	assert_string_equal(mem_ref[3], "1");
	assert_string_equal(mem_ref[4], "-o");
	assert_string_equal(mem_ref[5], "file");
}

static void _check_lock(void** state) {
	struct ffcommand* command = *state;
	char  mem[100]     = { '\0' };
	int _s = 0;

	ffcommand_add_audio(command, "A", "a", FFORD_TAIL);
	ffcommand_add_audio(command, "B", "b", FFORD_HEAD);
	ffcommand_add_audio(command, "C", "c", FFORD_TAIL);
	ffcommand_add_video(command, "V", "v", FFORD_TAIL);

	srand(time(NULL));
	int max = 0;
	for(int i = 0; i < 10; i++) {
		max = rand() % 100;
		for(int k = 0; k < max; k++) {
			_s = ffcommand_lock(command);
			assert_int_equal(_s, 0);

			ffcommand_add_output(command, "LOCK_OUTPUT", NULL, FFORD_TAIL);
			ffcommand_add_audio(command, "LOCK", NULL, FFORD_TAIL);
			ffcommand_add_input(command, "LOCK_INPUT", "FILE", FFORD_TAIL);

			ffcommand_export_string(command, mem, 100);
			assert_string_equal(mem, " LOCK_INPUT FILE V v B b A a C c LOCK LOCK_OUTPUT");

#ifndef MEMORY_TEST_HARD
			_s = ffcommand_unlock(command, FFPAGE_HOLD);
#else
			_s = ffcommand_unlock(command, FFPAGE_DELETE);
#endif
			assert_int_equal(_s, 0);
		}
	}
	for(int i = 0; i < 10; i++) {
		if(i == 2)
			ffcommand_add_video(command, "_OPTION_", "_VALUE_", FFORD_HEAD);
		if(i == 4)
			ffcommand_add_video(command, "_O_", "_V_", FFORD_HEAD);
		for(int k = 0; k < max; k++) {
			_s = ffcommand_lock(command);
			assert_int_equal(_s, 0);

			if(i < 2)
				assert_int_equal(command->allocator->page->page_number, 2);
			else if(i < 4)
				assert_int_equal(command->allocator->page->page_number, 3);
			else 
				assert_int_equal(command->allocator->page->page_number, 4);

			ffcommand_add_output(command, "LOCK_OUTPUT", NULL, FFORD_TAIL);
			ffcommand_add_audio(command, "LOCK", NULL, FFORD_TAIL);
			ffcommand_add_input(command, "LOCK_INPUT", "FILE", FFORD_TAIL);

			ffcommand_export_string(command, mem, 100);
			if(i >= 4) {
				assert_string_equal(mem, " LOCK_INPUT FILE _O_ _V_ _OPTION_ _VALUE_ "
						"V v B b A a C c LOCK LOCK_OUTPUT");
			} else if(i >= 2) {
				assert_string_equal(mem, " LOCK_INPUT FILE _OPTION_ _VALUE_ "
						"V v B b A a C c LOCK LOCK_OUTPUT");
			} else {
				assert_string_equal(mem, " LOCK_INPUT FILE V v B b A a C c LOCK LOCK_OUTPUT");
			}

#ifndef MEMORY_TEST
			_s = ffcommand_unlock(command, FFPAGE_HOLD);
#else
			_s = ffcommand_unlock(command, FFPAGE_DELETE);
#endif
			assert_int_equal(_s, 0);
		}
	}
}



int startup(void** state) {
	*state = malloc(sizeof(struct ffcommand));
	if(*state == NULL)
		return 1;
	if(ffcommand_init(*state))
		return 2;
	return 0;
}

int teardown(void** state) {
	ffcommand_destroy(*state);
	free(*state);
	return 0;
}



#define m_add_unit(func) cmocka_unit_test_setup_teardown(func, startup, teardown)

int main(int argc, char** argv) {
	const struct CMUnitTest tests[] = {
		m_add_unit(_check_add_functions),
		m_add_unit(_check_empty_command),
		m_add_unit(_check_export),
		m_add_unit(_check_lock),
	};

	return cmocka_run_group_tests(tests, NULL, NULL);
}


#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <time.h>
#include <setjmp.h>
#include <math.h>
#include <cmocka.h>

#include <video-clib/mm/allocator.h>

#define PAGE_SIZE 2048
#define MY__WORDSIZE (__WORDSIZE / 8)


static void _check_like_malloc(void** state) {
	struct allocator* const al = *state;

	void* mem = allocator_malloc(al, 0);
	assert_ptr_equal(mem, NULL);

	mem = allocator_malloc(al, 2*PAGE_SIZE);
	assert_ptr_equal(mem, NULL);
}

static void _check_pointer_validity(void** state) {
	struct allocator* const al = *state;
	void* mem_cache;

	void* mem = allocator_malloc(al, 35);
	mem_cache = mem;
	// Align check...
	assert_int_equal((uintptr_t)mem % MY__WORDSIZE, 0);

	mem_cache = allocator_malloc(al, 1);
	mem       = allocator_malloc(al, 1);
	assert_int_equal((uintptr_t)mem % MY__WORDSIZE, 0);
	assert_ptr_equal(mem_cache + MY__WORDSIZE, mem);
}

static void _check_max(void** state) {
	struct allocator* const al = *state;

	void* mem = allocator_malloc(al, PAGE_SIZE + 1);
	assert_ptr_equal(mem, NULL);

	mem = allocator_malloc(al, PAGE_SIZE);
	assert_ptr_equal(al->page->mem, mem + PAGE_SIZE);
}

static void _check_newpage(void** state) {
	struct allocator* const al = *state;

	int max = rand() % 569 + 10;

	for(int i = 0; i < max; i++) {
		allocator_malloc(al, PAGE_SIZE / 3);
		assert_int_equal(al->page->page_number, i);
		allocator_malloc(al, PAGE_SIZE / 2);
		assert_int_equal(al->page->page_number, i);
	}
}

static void _check_edge(void** state) {
	struct allocator* const al = *state;

	allocator_malloc(al, 159 % MY__WORDSIZE);

	int max = rand() % 9999 + 1000;
	int k   = 0;
	for(int i = 0; i <  max; i++) {
		void* mem_cache = al->page->mem;
		void* mem_ref = allocator_malloc(al, 1);
		if(al->page->used != k) {
			k++;
			continue;
		}
		assert_ptr_equal((uint8_t*)mem_cache, mem_ref);
	}
}

static void _check_lock(void** state) {
	struct allocator* const al = *state;

	allocator_page_lock(al);
	int max = rand() % 100 + 10;
	for(int i = 0; i < max; i++) {
		void* mem;
		mem	= allocator_malloc(al, PAGE_SIZE / 3);
		if(i > 0)
			assert_ptr_equal(mem, NULL);
		mem = allocator_malloc(al, PAGE_SIZE / 2);
		assert_ptr_equal(mem, NULL);
	}
	allocator_page_unlock(al);

	max = rand() % 100 + 10;
	for(int i =0; i < max; i++) {
		void* mem =  allocator_malloc(al, PAGE_SIZE / 3);
		if(i > 0)
			assert_ptr_not_equal(mem, NULL);
		mem = allocator_malloc(al, PAGE_SIZE / 2);
		assert_ptr_not_equal(mem, NULL);
	}
}

static void _check_state(void** state) {
	struct allocator* const al = *state;
	struct allocator_state st[100] = { ALLOCATOR_PAGE_STATIC_INIT(0) };
	int _s;

	int max;
	while((max = rand() % 100) && max % 3 > 0);

	int i;
	for(i = 0; i < max; i++) {
		st[i] = ALLOCATOR_PAGE_STATIC_INIT(rand() % PAGE_SIZE);
		assert_true(!ALLOCATOR_STATE_VALID(&st[i]));
	}
	for(i = 0; i < max; i++) {
		void* mem = allocator_malloc(al, (PAGE_SIZE / 3) - (PAGE_SIZE / 3) % MY__WORDSIZE);
		assert_ptr_not_equal(mem, NULL);

		_s = allocator_state_save(al, &st[i]);
		assert_true(!_s);

		assert_true(ALLOCATOR_STATE_VALID(&st[i]));
	}

	int round  = max / 3;
	int offset = max % 3;
	if(offset) {
		for(int k = 0; k < offset; k++)
			assert_true(!allocator_state_restore(al, &st[max - 1 - k]));
		assert_true(!allocator_delete_lastpage(al));
	}

	for(int k = round; k > 0; k--) {
		for(int l = 0; l < 3; l++)
			allocator_state_restore(al, &st[3*k - 1 - l]);
		allocator_delete_lastpage(al);
	}
}



int startup(void** state) {
	*state = malloc(sizeof(struct allocator));
	if(*state == NULL)
		return 1;
	if(allocator_init(*state, PAGE_SIZE))
		return 2;
	return 0;
}

int teardown(void** state) {
	allocator_destroy(*state);
	free(*state);
	return 0;
}

int prepare(void** state) {
	srand(time(NULL));
	return 0;
}



#define m_add_unit(func) cmocka_unit_test_setup_teardown(func, startup, teardown)

int main(int argc, char** argv) {
	const struct CMUnitTest tests[] = {
		m_add_unit(_check_like_malloc),
		m_add_unit(_check_pointer_validity),
		m_add_unit(_check_edge),
		m_add_unit(_check_max),
		m_add_unit(_check_lock),
		m_add_unit(_check_newpage),
		m_add_unit(_check_state),
	};

	return cmocka_run_group_tests(tests, prepare, NULL);
}

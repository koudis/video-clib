
#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <setjmp.h>
#include <string.h>
#include <cmocka.h>

#include <video-clib/parallel.h>
#include <video-clib/log.h>

#define NUM_THR 3

const char* log_context_name = "PARALLEL";



struct func_struct {
	int val;
	int sleep;
};

struct state_struct {
	struct parallel* parallel;
	char             testfile[100];
};

#define PARALLEL_JOB (struct parallel_job) {\
	.main = func, \
	.rsp  = response, \
};

#define SET_TEST_FILE(s, file) strcpy(s->testfile, file);

int get_child_process_count() {
	pid_t ppid = getpid();
	int count  = 0;
	char *buff = NULL;
	size_t len = 255;
	char command[256] = {0};

	sprintf(command,"ps -ef | awk '$3==%u {print $2}'", ppid);
	FILE *fp = (FILE*)popen(command,"r");
	while(getline(&buff,&len,fp) >= 0) {
		count++;
	}
	free(buff);
	fclose(fp);
	return count - 1;
}



int func(void* _a) {
	struct func_struct* fs = (struct func_struct*)_a;
	sleep(fs->sleep);
	char str[100];
	sprintf(str, "echo %i >> /tmp/test && sync", fs->val);
	system(str);
	return 0;
}

int response(struct parallel_report* rsp, struct parallel_job* job) {
	printf("PID %i\n", job->pid);
	fflush(stdout);
	return 0; // free stack
}



/**
 * Test file:
 *     >1
 *     >2
 *     >3
*/
void _check_parallel(void** state) {
	struct state_struct* s = *state;
	SET_TEST_FILE(s, "parallel.t");

	struct parallel* p      = s->parallel;
	struct parallel_job job = PARALLEL_JOB;
	struct func_struct arg  = {
		.val   = 0,
		.sleep = 0,
	};

	arg.sleep = 1; arg.val = 1;
	int _s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_true(!_s);

	arg.sleep = 2; arg.val = 2;
	_s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_true(!_s);

	arg.sleep = 3; arg.val = 3;
	_s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_true(!_s);

	_s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_int_equal(_s, EP_NEMEM);
	_s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_int_equal(_s, EP_NEMEM);

	parallel_wait(p, 1);
}

/**
 * Test file:
 *     >1
 *     >2
 *     >3
 *     >3
*/
void _check_parallel2(void** state) {
	struct state_struct* s = *state;
	SET_TEST_FILE(s, "parallel2.t");

	struct parallel* p      = s->parallel;
	struct parallel_job job = PARALLEL_JOB;
	struct func_struct arg  = {
		.val   = 0,
		.sleep = 0,
	};

	arg.sleep = 1; arg.val = 1;
	int _s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_true(!_s);

	arg.sleep = 2; arg.val = 2;
	_s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_true(!_s);

	arg.sleep = 3; arg.val = 3;
	_s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_true(!_s);

	parallel_wait(p, 0);

	_s = parallel_start(p, &job, CLONE_IO, &arg, 0);
	assert_true(!_s);
	_s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_int_equal(_s, EP_NEMEM);

	parallel_wait(p, 1);

}

/**
 * Test file:
 *     >1
 *     >2 
*/
void _check_wait(void** state) {
	struct state_struct* s = *state;
	SET_TEST_FILE(s, "parallel_wait.t");

	struct parallel* p      = s->parallel;
	struct parallel_job job = PARALLEL_JOB;
	struct func_struct arg  = {
		.val   = 0,
		.sleep = 0,
	};

	arg.sleep = 1; arg.val = 1;
	int _s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_true(!_s);

	arg.sleep = 2; arg.val = 2;
	_s = parallel_start(p, &job, CLONE_IO, &arg, 1);
	assert_true(!_s);

	parallel_wait(p, 1);
	assert_int_equal(get_child_process_count(), 0);
}



int startup(void** state) {
	struct parallel* p = malloc(sizeof(struct parallel));
	if(p == NULL)
		return 1;
	if(system("rm -f /tmp/test"))
		return 1;
	if(parallel_init(p, NUM_THR))
		return 2;
	
	struct state_struct* s = *state;
	s->parallel = p;

	return 0;
}

int teardown(void** state) {
	struct state_struct* s = *state;

	char str[100];
	sprintf(str, "diff /tmp/test /tmp/tt/%s", s->testfile);
	int _s = system(str);
	assert_true(!_s);

	if(system("rm -f /tmp/test"))
		return 1;

	parallel_destroy(s->parallel);
	free(s->parallel);
	return 0;
}



static char* testpath;

int startup_global(void** state) {
	*state = malloc(sizeof(struct func_struct));
	if(system("rm -rf /tmp/tt"))
		return 1;

	char str[250];
	sprintf(str, "mkdir /tmp/tt/ && cp %s/*.t /tmp/tt/", testpath);
	if(system(str))
		return 1;
	return 0;
}

int teardown_global(void** state) {
	struct state_struct* s = *state;

	if(system("rm -rf /tmp/tt"))
		return 1;

	free(s);
	return 0;
}



#define m_add_unit(func) cmocka_unit_test_setup_teardown(func, startup, teardown)

int main(int argc, char** argv) {
	if(argc < 2) {
		fputs("Please, set path....\n", stderr);
		return 1;
	}
	testpath = argv[1];

	const struct CMUnitTest tests[] = {
		m_add_unit(_check_parallel),
		m_add_unit(_check_parallel2),
		m_add_unit(_check_wait),
	};

	return cmocka_run_group_tests(tests, startup_global, teardown_global);
}

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdlib.h>
#include <stdio.h>
#include <cmocka.h>

#define assert(expression) \
	mock_assert((int)(expression), #expression, __FILE__, __LINE__);

void _assert_failed(const char* cond, int line, const char* file) {
	mock_assert(0, cond, file, line);
}

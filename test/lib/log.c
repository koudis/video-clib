
#include <sys/types.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <stdio.h>
#include <fcntl.h>
#include <setjmp.h>
#include <unistd.h>

#include <cmocka.h>

const char* log_context_name __attribute__((weak)) = "LOG";



inline static int
_log_wrapper(int l, const char* msg, va_list va_l) {

	FILE* fd = fopen("log.txt", "a+");
	if(!fd)
		fail_msg("Cannot open log file for reading");

	fprintf(fd, "[%s]\t", log_context_name);
	vfprintf(fd, msg, va_l);

	if(fclose(fd))
		fail_msg("Cannot close the file");

	return 0;
}



void
par_log_init() {
	unlink("log.txt");
}

int
par_log(int l, const char* msg, ...) {
	va_list va_l;
	va_start(va_l, msg);
	int _tmp = _log_wrapper(l, msg, va_l);
	va_end(va_l);
	return _tmp;
}

int
par_vlog(int l, const char* msg, va_list va_l) {
	return _log_wrapper(l, msg, va_l);
}

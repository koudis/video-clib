
#include <stdio.h>
#include <stdarg.h>

#include <video-clib/log.h>



struct log_struct log_struct = { 0 };

inline static int
_log_wrapper(enum log_level l, const char* msg, va_list va_l) {
	const struct log_struct* const ls = &log_struct;

	if(!ls->descriptor)
		par_log_init();

	int x = 0;
	if(ls->visible && ls->descriptor != stderr)
		x = vfprintf(ls->descriptor, msg, va_l);

	x = (vfprintf(ls->descriptor, msg, va_l) && vfprintf(ls->descriptor, "\n", va_l) && x);

	return x;
}



WEAK void
par_log_init() {
	log_struct = (struct log_struct) {
		.descriptor = stderr,
		.visible    = LOG_VISIBLE_DEFAULT
	};
}

WEAK int
par_log(enum log_level l, const char* msg, ...) {
	va_list va_l;
	va_start(va_l, msg);
	return _log_wrapper(l, msg, va_l);
}

WEAK int
par_vlog(enum log_level l, const char* msg, va_list va_l) {
	return _log_wrapper(l, msg, va_l);
}

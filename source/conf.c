
#include <config.h>

#include <yaml.h>
#include <Judy.h>
#include <stdio.h>
#include <stdarg.h>

#include <video-clib/macro.h>
#include <video-clib/mm/alloc.h>
#include <video-clib/mm/allocator.h>
#include <video-clib/log.h>
#include <video-clib/assert.h>
#include <video-clib/conf.h>

COMPILE_ASSERT(conf_index_max_length,
		CONF_INDEX_MAX_LENGTH > 0);
COMPILE_ASSERT(conf_allocator_page_size,
		CONF_ALLOCATOR_PAGE_SIZE > 0 &&
		CONF_ALLOCATOR_PAGE_SIZE % 2 == 0);



int
conf_init(struct conf* conf) {
	struct allocator* al = malloc(sizeof(struct allocator));
	int _tmp = allocator_init(al, CONF_ALLOCATOR_PAGE_SIZE);
	if(_tmp)
		LOG_AND_RETURN(L_ERROR, EC_ERROR, "Cannot init allocator");

	*conf = (struct conf) {
		.allocator = al,
		.judy      = NULL,
	};

	return 0;
}



inline static void
clean_array(void** judy);

void
conf_destroy(struct conf* conf) {

	clean_array(&conf->judy);

	int _tmp = allocator_destroy(conf->allocator);
	if(_tmp)
		die("Ou, cannot destroy conf allocator");
	free(conf->allocator);
}

inline static void
clean_array(void** judy) {
	uint8_t index[CONF_INDEX_MAX_LENGTH];
	PWord_t value;
	struct conf_value* conf_value;

	index[0] = '\0';
    JSLF(value, *judy, index);
	while (value != NULL) {
		conf_value = (struct conf_value*)(*value);
		if(conf_value->type == CONF_BLOCK_MAPPING)
			clean_array(&conf_value->value);
		if(conf_value->type == CONF_BLOCK_SEQUENCE)
			clean_array(&conf_value->value);
		JSLN(value, *judy, index);
	}

	Word_t bytes;
	JSLFA(bytes, *judy);
}




static int 
conf_process_yaml_mapping(struct conf* conf, void** judy,
		yaml_parser_t* parser, int depth);

static int 
conf_process_yaml_sequence(struct conf* conf, void** judy,
		yaml_parser_t* parser, int depth);

int
conf_load(struct conf* conf, FILE* fd) {
	yaml_parser_t parser;

	if (!yaml_parser_initialize(&parser))
		LOG_AND_RETURN(L_ERROR, EC_YPE, "Cannot init yaml parser");

	yaml_parser_set_input_file(&parser, fd);

	int _tmp = conf_process_yaml_mapping(conf, &conf->judy, &parser, 0);
	yaml_parser_delete(&parser);

	if(_tmp) {
		conf_destroy(conf);
		LOG_AND_RETURN(L_ERROR, EC_IFRM, "Cannot process given yaml file");
	}

	return 0;
}

int
conf_loadf(struct conf* conf, const char* const file) {
	FILE* fh = fopen(file, "r");
	if (fh == NULL)
		LOG_AND_RETURN(L_ERROR, EC_ENOENT, "Cannot open file %s", file);

	int _tmp = conf_load(conf, fh);
	fclose(fh);

	return _tmp;
}




#define ADD_STATE(var, state) (var[2] = var[1], var[1] = var[0],  var[0] = state)
#define STATE(var) (var[0])
#define STATE_CONTEXT(var) (var[1])
#define STATE_INVALID_CONTEXT(var) (var[2] == var[0])

#define INIT_CONF_VALUE(al, judy_value, conf_value, _type, _val) do {\
					conf_value = allocator_malloc(al, \
							sizeof(struct conf_value)); \
					conf_value->type  = _type; \
					conf_value->value = (void*)_val; \
					*judy_value = (void*) conf_value; \
					} while(0)

static int
conf_yaml_parser_scan(yaml_parser_t* parser, yaml_token_t* token);

static int 
conf_process_yaml_mapping(struct conf* conf, void** judy,
		yaml_parser_t* parser, int depth) {

	yaml_token_t token;
	int state[3] = { 0 };
	int _tmp;
	char*  tk;
	void** value = NULL;
	struct conf_value* conf_value;

	do {
		conf_yaml_parser_scan(parser, &token);
		switch(token.type) {
			case YAML_KEY_TOKEN:
				ADD_STATE(state, YAML_KEY_TOKEN);
				yaml_token_delete(&token);
				if(STATE_INVALID_CONTEXT(state))
					return EC_IFRM;
				continue;
			case YAML_VALUE_TOKEN:
				ADD_STATE(state, YAML_VALUE_TOKEN);
				yaml_token_delete(&token);
				if(STATE_INVALID_CONTEXT(state))
					return EC_IFRM;
				continue;

			case YAML_FLOW_SEQUENCE_START_TOKEN:
			case YAML_BLOCK_SEQUENCE_START_TOKEN:
				ADD_STATE(state, YAML_BLOCK_SEQUENCE_START_TOKEN);
				goto process_sequence;
			case YAML_FLOW_MAPPING_START_TOKEN:
			case YAML_BLOCK_MAPPING_START_TOKEN:
				ADD_STATE(state, YAML_BLOCK_MAPPING_START_TOKEN);
				goto process_mapping;
			case YAML_SCALAR_TOKEN:
				ADD_STATE(state, YAML_SCALAR_TOKEN);
				goto process_scalar;

			case YAML_STREAM_END_TOKEN:
			case YAML_NO_TOKEN:
				if(depth) {
					yaml_token_delete(&token);
					return EC_IFRM;
				}
			case YAML_FLOW_MAPPING_END_TOKEN:
			case YAML_BLOCK_END_TOKEN:
				yaml_token_delete(&token);
				return 0;

			default:
				yaml_token_delete(&token);
				continue;
		}

		process_sequence:
			yaml_token_delete(&token);
			INIT_CONF_VALUE(conf->allocator, value, conf_value,
				CONF_BLOCK_SEQUENCE, NULL);
			_tmp = conf_process_yaml_sequence(conf, &conf_value->value, parser, depth + 1);
			if(_tmp)
				return _tmp;
			continue;

		process_mapping:
			yaml_token_delete(&token);
			if(STATE_CONTEXT(state) != YAML_VALUE_TOKEN)
				continue;
			INIT_CONF_VALUE(conf->allocator, value, conf_value,
				CONF_BLOCK_MAPPING, NULL);
			_tmp = conf_process_yaml_mapping(conf, &conf_value->value, parser, depth + 1);
			if(_tmp)
				return _tmp;
			continue;

		process_scalar:
			tk = (char*)token.data.scalar.value;
			if(STATE_CONTEXT(state) == YAML_KEY_TOKEN) {
				ASSERT(strlen(tk) < CONF_INDEX_MAX_LENGTH);

				PWord_t judy_value;
				JSLI(judy_value, *judy, (uint8_t*)tk);
				value = (void*)judy_value;
				*value = NULL;
			}
			if (STATE_CONTEXT(state) == YAML_VALUE_TOKEN) {
				void* valmemref = allocator_malloc(conf->allocator, strlen(tk));
				strcpy((char*)valmemref, tk);

				INIT_CONF_VALUE(conf->allocator, value, conf_value,
					CONF_SCALAR, valmemref);
			}
			yaml_token_delete(&token);
	} while(1);
	
	return 0;
}

/**
 *
 *
*/
static int 
conf_process_yaml_sequence(struct conf* conf, void** judy,
		yaml_parser_t* parser, int depth) {

	yaml_token_t token;
	int state[3] = { 0 };
	int i = -1;
	void** value = NULL;
	struct conf_value* conf_value;
	char key[2*CONF_INDEX_MAX_LENGTH];
	char* tk;
	int _tmp;

	do {
		conf_yaml_parser_scan(parser, &token);
		switch(token.type) {
			case YAML_FLOW_ENTRY_TOKEN:
			case YAML_BLOCK_ENTRY_TOKEN:
				ADD_STATE(state, YAML_BLOCK_ENTRY_TOKEN);
				goto process_key;

			case YAML_FLOW_MAPPING_START_TOKEN:
			case YAML_BLOCK_MAPPING_START_TOKEN:
				ADD_STATE(state, YAML_BLOCK_MAPPING_START_TOKEN);
				goto process_mapping;

			case YAML_FLOW_SEQUENCE_START_TOKEN:
			case YAML_BLOCK_SEQUENCE_START_TOKEN:
				ADD_STATE(state, YAML_BLOCK_SEQUENCE_START_TOKEN);
				goto process_sequence;

			case YAML_SCALAR_TOKEN:
				ADD_STATE(state, YAML_SCALAR_TOKEN);
				goto process_scalar;

			case YAML_STREAM_END_TOKEN:
			case YAML_NO_TOKEN:
				if(depth) {
					yaml_token_delete(&token);
					return EC_IFRM;
				}
			case YAML_FLOW_SEQUENCE_END_TOKEN:
			case YAML_BLOCK_END_TOKEN:
				yaml_token_delete(&token);
				return 0;

			default:
				yaml_token_delete(&token);
				continue;
		}

		process_key:
			i++;
			sprintf(key, "%d", i);

			PWord_t judy_value;
			JSLI(judy_value, *judy, (uint8_t*)key);
			value = (void**)judy_value;
			if(STATE(state) == YAML_SCALAR_TOKEN)
				goto process_scalar;
			yaml_token_delete(&token);
			continue;

		process_mapping:
			yaml_token_delete(&token);
			INIT_CONF_VALUE(conf->allocator, value, conf_value,
				CONF_BLOCK_MAPPING, NULL);
			_tmp = conf_process_yaml_mapping(conf, &conf_value->value, parser, depth + 1);
			if(_tmp)
				return _tmp;
			continue;

		process_sequence:
			yaml_token_delete(&token);
			if(STATE_CONTEXT(state) != YAML_VALUE_TOKEN)
				continue;
			INIT_CONF_VALUE(conf->allocator, value, conf_value,
				CONF_BLOCK_SEQUENCE, NULL);
			int _tmp = conf_process_yaml_sequence(conf, &conf_value->value, parser, depth + 1);
			if(_tmp)
				return _tmp;
			continue;

		process_scalar:
			if(value == NULL)
				goto process_key;
			tk = (char*)token.data.scalar.value;
			char* valmemref = allocator_malloc(conf->allocator, strlen(tk));
			strcpy(valmemref, tk);

			INIT_CONF_VALUE(conf->allocator, value, conf_value,
				CONF_SCALAR, (void*)valmemref);
			value = NULL;
			yaml_token_delete(&token);
			continue;
	} while(1);

	return 0;
}

static int
conf_yaml_parser_scan(yaml_parser_t* parser, yaml_token_t* token) {
	yaml_parser_scan(parser, token);
	switch(token->type) {
		case YAML_ALIAS_TOKEN:
		case YAML_ANCHOR_TOKEN:
		case YAML_TAG_TOKEN:
			LOG_AND_RETURN(L_ERROR, EC_UNSUP, "Ou, Alliases, anchors and tags are not supported");
		default:
			return 0;
	}
}



void*
conf_getc(struct conf* conf, unsigned int num, ...) {
	va_list valist;
	PWord_t value;

	va_start(valist, num);

	struct conf_value* conf_value = NULL;
	void* judy = conf->judy;
	int i;
	for(i = 1; i <= num; i++) {
		uint8_t* key = va_arg(valist, uint8_t*);
		JSLG(value, judy, key);
		if(value == NULL)
			break;

		conf_value = (void*)*value;
		if(conf_value->type == CONF_SCALAR && i != num)
			break;
		judy = conf_value->value;
	}

	va_end(valist);
	if(i <= num)
		return NULL;

	return conf_value->value;
}

void*
conf_getj(void* judy, unsigned int num, ...) {
	va_list valist;
	PWord_t value;

	va_start(valist, num);

	struct conf_value* conf_value = NULL;
	int i;
	for(i = 1; i <= num; i++) {
		uint8_t* key = va_arg(valist, uint8_t*);
		JSLG(value, judy, key);
		if(value == NULL)
			break;

		conf_value = (void*)*value;
		if(conf_value->type == CONF_SCALAR && i != num)
			break;
		judy = conf_value->value;
	}

	va_end(valist);
	if(i <= num)
		return NULL;

	return conf_value->value;
}

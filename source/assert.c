
#include <stdarg.h>
#include <video-clib/assert.h>
#include <video-clib/error.h>
#include <video-clib/log.h>


void 
_assert_failed(const char* cond, int line,
		const char* file) {
	par_log(L_CRIT, "assert '%s' failed. File '%s' on line '%i'",
			cond, file, line);
	exit(EXIT_FAILURE);
}

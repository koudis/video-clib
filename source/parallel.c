
#include <config.h>

#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h> // va_list
#include <string.h>
#include <fcntl.h> // O_*
#include <unistd.h> // SEEK_*, 
#include <sys/types.h> // pid_t, some pthread_*, ...
#include <sys/mman.h>
#include <sys/stat.h> // S_*
#include <sys/resource.h> // rlimit

#include <video-clib/parallel.h>
#include <video-clib/assert.h>
#include <video-clib/error.h>
#include <video-clib/mm/alloc.h>
#include <video-clib/mm/mmap.h>
#include <video-clib/mm/allocator.h>
#include <video-clib/macro.h>
#include <video-clib/log.h>

#define PARALLEL_CLONE_FLAGS CLONE_IO

#define PARALLEL_CLONE_FLAGS_AVAILABLE CLONE_IO



static void
par_pthread_die(const char* message, ...) {
	va_list va_l;
	va_start(va_l, message);

	par_vlog(L_CRIT, message, va_l);
	void* retval = EXIT_FAILURE;
	pthread_exit(retval);
}

static int
par_pthread_join(pthread_t thread, void** retval) {
	int _s = pthread_join(thread, retval);
	if(_s)
		par_pthread_die("pthread_join failed");
	return _s;
}

static int
par_pthread_mutex_lock(pthread_mutex_t* mutex) {
	int _s = pthread_mutex_lock(mutex);
	if(_s)
		par_pthread_die("pthread_mutex_lock (%i)", _s);
	return _s;
}

static int
par_pthread_mutex_unlock(pthread_mutex_t* mutex) {
	int _s = pthread_mutex_unlock(mutex);
	if(_s)
		par_pthread_die("pthread_mutex_unlock (%i)", _s);
	return _s;
}







/**
 * @brief Init parallel_wait struct
 * Caller can not call this function. This is done by parallel_init
 *
 * @param pw struct parallel_wait
 * @return 
*/
static int
parallel_init_wait(struct parallel_wait* pw, struct allocator* _al) {
	*pw = (struct parallel_wait) {
		.thread = allocator_malloc(_al, sizeof(pthread_t)),
		.attr   = allocator_malloc(_al, sizeof(pthread_attr_t)),
		.pick   = par_mmap("/tmp/", sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED),
		.run = 0,
	};
	if(IS_ERR(pw->pick))
		die("Nooo, cannot map shared memory for semaphore :(");

	if(sem_init(pw->pick,1,0))
		die("Cannot init pick semaphore");

	if(pthread_attr_init(pw->attr))
		die("Cannot init attr for w thread");
	if(pthread_attr_setdetachstate(pw->attr, PTHREAD_CREATE_JOINABLE))
		die("Cannot set detachstate for thread");

	return 0;
}

/**
 * @brief
 * Caller can not (should not) call this function. This is done by parallel_destroy
 *
 * -# wait on all running childs
 * -# for each stoped child execute parallel_report function
 *    (rsp in parallel_job)
 * -# destroy parallel struct
 *
 * @param pw struct parallel_wait
*/
static void
parallel_destroy_wait(struct parallel_wait* pw) {
	if(pthread_attr_destroy(pw->attr))
		die("cannot destroy attr object");
	if(sem_destroy(pw->pick))
		die("cannotdestroy pick sem");
	if(munmap(pw->pick, sizeof(sem_t)))
		die("Cannot unmap memory for pick sem");
}






int
parallel_init(struct parallel* p, uint8_t max) {
	struct allocator* _al = xmalloc(sizeof(struct allocator));
	if(allocator_init(_al, 0))
		die("Cannot init allocator");

	struct rlimit rlimit;
	if(getrlimit(RLIMIT_STACK, &rlimit))
		die("Cannot get STACK size");

	*p = (struct parallel) {
		.allocator = _al,
		.jobs      = allocator_malloc(_al, max*sizeof(struct parallel_job*)),
		.wait      = allocator_malloc(_al, sizeof(struct parallel_wait)),
		.stack = {
			.size  = rlimit.rlim_cur,
			.inuse = allocator_malloc(_al, max*sizeof(void*)),
			.free  = allocator_malloc(_al, max*sizeof(void*)),	
		},
		.mutex = allocator_malloc(_al, sizeof(pthread_mutex_t)),
		.sem   = allocator_malloc(_al, sizeof(sem_t)),
		.max   = max,
	};

	memset(p->jobs, 0, max*sizeof(struct parallel_job*));

	pthread_mutexattr_t mutex_attr;
	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_setprotocol(&mutex_attr, PTHREAD_MUTEX_FAST_NP);
	if(pthread_mutex_init(p->mutex, &mutex_attr))
		die("Cannot init mutex");

	sem_init(p->sem, 0, max);

	parallel_init_wait(p->wait, _al);

	return 0;
}



void
parallel_destroy(struct parallel* p) {
	int _s = parallel_wait(p, 1);
	parallel_destroy_wait(p->wait);
	for(int i = 0; i < p->max; i++) {
		ASSERT(p->stack.inuse[i] == NULL);
		if(p->stack.free[i] == NULL)
			continue;
		xfree(p->stack.free[i]);
		p->stack.free[i]  = NULL;
		p->stack.inuse[i] = NULL;
	}

	if(pthread_mutex_destroy(p->mutex))
		die("Cannot destroy mutex");

	if(sem_destroy(p->sem))
		die("Cannot destroy semaphore");

	_s = allocator_destroy(p->allocator);
	if(_s)
		die("Cannot destroy allocator");
	xfree(p->allocator);
}



struct start {
	struct parallel_wait* wait;
	struct parallel_job* job;
	void* arg;
};

static int _start(void* _a);

int
parallel_start(struct parallel* p, struct parallel_job* job,
		int cflags, void* a, int noblock) {
	ASSERT(p != NULL);
	ASSERT(job != NULL);

	if(!noblock) {
		sem_wait(p->sem);
	} else {
		int _s = sem_trywait(p->sem);
		if(_s == -1 && errno == EAGAIN)
			return EP_NEMEM;
	}

	if((cflags & PARALLEL_CLONE_FLAGS_AVAILABLE) != cflags)
		LOG_AND_RETURN(L_ERROR, EP_ERROR, "Unsupported clone flag(s)! (%i)", cflags);

	par_pthread_mutex_lock(p->mutex);

	void* stack = NULL;
	void** inuse = p->stack.inuse;
	void** free  = p->stack.free;
	int i;
	for(i = 0; i < p->max; i++) {
		if(inuse[i] == NULL && free[i] == NULL) {
			inuse[i] = xmalloc(p->stack.size);
			free[i]  = NULL;
			stack    = inuse[i];
			break;
		} else if(inuse[i] == NULL) {
			ASSERT(free[i] != NULL);
			inuse[i] = free[i];
			stack    = free[i];
			break;
		}

	}
	if(stack == NULL) {
		par_pthread_mutex_unlock(p->mutex);
		LOG_AND_RETURN(L_ERROR, EP_ERROR, "Max running childs (%i) reached!", p->max);
	}

	struct parallel_job* _job = p->jobs[i];
	if(_job == NULL) {
		p->jobs[i] = allocator_malloc(p->allocator, sizeof(struct parallel_job));
		_job = p->jobs[i];
	}

	int pid;
	struct start start = (struct start) {
		.job  = job,
		.wait = p->wait,
		.arg  = (void*)a,
	};
	par_pthread_mutex_unlock(p->mutex);
	pid = clone(_start, stack + p->stack.size, PARALLEL_CLONE_FLAGS, &start);
	if(pid == -1)
		LOG_AND_RETURN(L_ERROR, EP_CCLN, "Cannot clone");

	*job = (struct parallel_job) {
		.main  = job->main,
		.rsp   = job->rsp,
		.stack = stack,
		.pid   = pid,
	};
	ASSERT_WRITEABLE((void *)_job);
	*_job = *job;

	return 0;
}

static int
_start(void* _a) {
	struct start const* a = (struct start*)_a;
	int _s = a->job->main(a->arg);
	sem_post(a->wait->pick);
	return _s;
}



static void* _wait(void*);
static int  _find_and_pick(struct parallel* p);
static int  _free_stack(struct parallel* p, struct parallel_job* job);

int
parallel_wait(struct parallel* p, int block) {
	ASSERT(p != NULL);

	par_pthread_mutex_lock(p->mutex);
	if(!p->wait->run) {
		int tmp = pthread_create(p->wait->thread, p->wait->attr, _wait, p);
		if(tmp)
			LOG_AND_RETURN(L_ERROR, EP_CCWT, "Cannot create waiting thread");
		p->wait->run = 1;
		par_pthread_mutex_unlock(p->mutex);

		if(block) {
			void* retval;
			par_pthread_join(*p->wait->thread, &retval);
			p->wait->run = 0;
			return (long)retval;
		}
		return 0;
	}
	par_pthread_mutex_unlock(p->mutex);
	
	if(block) {
		void* retval;
		par_pthread_join(*p->wait->thread, &retval);
		p->wait->run = 0;
		return (long)retval;
	}

	return 0;
}



void*
_wait(void* _p) {
	struct parallel* p = (struct parallel*)_p;

	int _s = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	if(_s)
		 par_pthread_die("Cannot set pthreadcancelstate (%i)", _s);

	par_pthread_mutex_lock(p->mutex);

	int semval;
	if(sem_getvalue(p->sem, &semval))
		par_pthread_die("sem_getvalue failed");
	if(semval == p->max) {
		par_pthread_mutex_unlock(p->mutex);
		return NULL;
	}

	par_pthread_mutex_unlock(p->mutex);

	while(1) {
		sem_wait(p->wait->pick);

		_find_and_pick(p);

		int _s = sem_post(p->sem);
		if(_s)
			par_pthread_die("sem_post (%i)", _s);

		par_pthread_mutex_lock(p->mutex);
		if(sem_getvalue(p->sem, &semval))
			par_pthread_die("sem_getvalue failed");
		if(semval == p->max)
			break;
		par_pthread_mutex_unlock(p->mutex);
	}

	par_pthread_mutex_unlock(p->mutex);
	return NULL;
}

int
_find_and_pick(struct parallel* p) {
	int k = -1, status = 0;
	pid_t wpid;
	struct parallel_job** jobs = p->jobs;
	struct parallel_report report;

	int found = 0;
	while(!found) {
		k++;
		if(k == p->max)
			k = 0;

	 	if(p->stack.inuse[k] == NULL)
			continue;

		wpid = waitpid(jobs[k]->pid, &status, __WCLONE | WNOHANG);
		if(wpid == 0)
			continue;

		found = 1;

		report = (struct parallel_report) {
			.status = status,
			.verrno = errno,
			.wpid   = wpid,
		};

		// outside the lock. Just let the user choose...
		int _s = jobs[k]->rsp(&report, jobs[k]);
		if(!_s) {
			_s = _free_stack(p, jobs[k]);
			if(_s)
				par_pthread_die("Fatrtal error. Cannot freed the stack for process %i", jobs[k]->pid);
		}
	}
	return 0;
}

int
_free_stack(struct parallel* p, struct parallel_job* job) {
	int i;
	par_pthread_mutex_lock(p->mutex);
	for(i = 0; i < p->max; i++) {
		if(p->stack.inuse[i] == NULL)
			continue;
		if(p->stack.inuse[i] == job->stack) {
			p->stack.free[i]  = p->stack.inuse[i];
			p->stack.inuse[i] = NULL;
			par_pthread_mutex_unlock(p->mutex);
			return 0;
		}
	}
	par_pthread_mutex_unlock(p->mutex);
	return 1;
}

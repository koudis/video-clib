
#include <config.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <math.h>

#include <video-clib/mm/alloc.h>
#include <video-clib/mm/allocator.h>
#include <video-clib/macro.h>
#include <video-clib/log.h>
#include <video-clib/assert.h>



/**
 * alocate a new page in "list"
 * @see struct allocator
*/
static int _create_new_page(struct allocator* al) {
	struct allocator_page* const page = al->page;
	if(al->lock)
		return 1;

	uint8_t* new_page = xmalloc(al->page->size);

	list_append(al->list, new_page);
	page->mem         = new_page;
	page->used        = 0;
	page->page_number += 1;

	return 0;
}



int allocator_init(struct allocator* al, const size_t page_size) {
	size_t size;
	switch(page_size) {
		case 0:
#ifdef HAVE_SC_PAGESIZE
		size = sysconf(_SC_PAGESIZE); break;
#else
		size = ALLOCATOR_DEFAULT_PAGE_SIZE; break;
#endif
		default: size = page_size;
	}	

	*al = (struct allocator) {
		.list = xmalloc(sizeof(list_t)),
		.page = xmalloc(sizeof(struct allocator_page)),
		.lock = 0,
	};

	*al->page = ALLOCATOR_PAGE_STATIC_INIT(size);
	if(list_init(al->list))
		LOG_AND_RETURN(L_CRIT, -1, "list_init failed");
	return 0;
}

int allocator_destroy(struct allocator* al) {
	list_t* const list = al->list;

	list_iterator_start(list);
	while(list_iterator_hasnext(list)) {
		uint8_t* const mem = list_iterator_next(list);
		free(mem);
	}
	list_iterator_stop(list);

	list_destroy(list);
	free(al->page);
	free(al->list);
	return 0;
}



#ifndef HAVE__WORDSIZE
#define __WORDSIZE ALLOCATOR_WORD_SIZE
#endif

void* allocator_malloc(struct allocator* al, const size_t size) {
	if(!size)
		return NULL;
	if(size > al->page->size) 
		LOG_AND_RETURN(L_ERROR, NULL,
				"Attempt to allocate more then "
				"%lu (ALLOCATOR_PAGE_SIZE) bytes (%li)",
				al->page->size, size);

	size_t alloc_size = size;

	// TODO (this not ensure aligned access)
	if(alloc_size  % (__WORDSIZE/8) != 0)
		alloc_size = alloc_size + (__WORDSIZE/8) - (alloc_size % (__WORDSIZE/8));

	struct allocator_page* const page = al->page;
	void* mem;
	while(1) {
		if(page->size - page->used >= alloc_size) {
			mem        = page->mem;
			page->mem  = (uint8_t*)page->mem + alloc_size;
			page->used += alloc_size; 
			return mem;
		} else {
			if(_create_new_page(al))
				LOG_AND_RETURN(L_CRIT, NULL, "_create_new_page failed");
		}
	}
}



int allocator_state_save(struct allocator* al, struct allocator_state* st) {
	if(al->lock)
		return 1;
	if(unlikely(ALLOCATOR_STATE_VALID(st)))
		return 1;
	*st = *al->page;
	return 0;
}

int allocator_state_restore(struct allocator* al, struct allocator_state* st) {
	if(al->lock)
		return 1;
	ASSERT((uintptr_t)al->page->mem - ((uintptr_t)st->mem - st->used) >= 0);
	ASSERT((uintptr_t)al->page->mem - ((uintptr_t)st->mem - st->used) <= st->size);
	*al->page = *st;
	*st = ALLOCATOR_PAGE_STATIC_INIT(0);
	return 0;
}



int allocator_page_lock(struct allocator* al) {
	al->lock = 1;
	return 0;
}

int allocator_page_unlock(struct allocator* al) {
	al->lock = 0;
	return 0;
}



int allocator_cleanpage(struct allocator* al) {
	struct allocator_page* page = al->page;
	page->mem  = ALLOCATOR_PAGE_START(page);
	page->used = 0;
	return 0;
}

int allocator_create_newpage(struct allocator* al) {
	if(al->lock)
		return 1;
	if(ALLOCATOR_IS_PAGE_CLEAN(al->page))
		return 0;

	if(_create_new_page(al))
		LOG_AND_RETURN(L_CRIT, EA_ERROR, "Allocator_create_newpage failed");

	return 0;
}

int allocator_delete_lastpage(struct allocator* al) {
	unsigned int size = list_size(al->list);
	if(al->lock)
		return 1;
	if(size == 0)
		return 0;

	void* mem = (void*)list_get_at(al->list, size - 1);
	if(mem == NULL) {
		par_log(L_CRIT, "Ou, cannot fetch 'last' page in allocator");
		return 1;
	}

	xfree(mem);

	list_delete_at(al->list, size - 1);
	size -= 1;

	if(size == 0) {
		*al->page = ALLOCATOR_PAGE_STATIC_INIT(al->page->size);
		return 0;
	}

	mem = (void*)list_get_at(al->list, size - 1);
	if(mem == NULL) {
		par_log(L_CRIT, "Ou, cannot fetch current page in allocator");
		return 1;
	}
	al->page->mem = mem;

	return 0;
}

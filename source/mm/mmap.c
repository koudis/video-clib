
#include <config.h>

#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h> // S_*
#include <fcntl.h> // O_*
#include <sys/mman.h>
#include <video-clib/mm/mmap.h>
#include <video-clib/error.h>

void* par_mmap(const char* const name, size_t length, int prot, int flags) {
	mode_t mode = 0;
	if(prot & PROT_EXEC)
		mode |= S_IXUSR;
	if(prot & PROT_WRITE)
		mode |= S_IWUSR;
	if(prot & PROT_READ)
		mode |= S_IRUSR; 

	int file = open(name, O_RDWR | O_TMPFILE | O_TRUNC, S_IRWXU);
	if(file == -1)
		return ERR_PTR(-MM_MCOPS);

	int _s = ftruncate(file, length);
	if(_s)
		return ERR_PTR(-MM_MTRNC);
	if(syncfs(file) == -1)
		return ERR_PTR(-MM_MSFAI);

	void* mm = mmap(NULL, length, prot, flags, file, 0);
	if(mm == MAP_FAILED)
		return MAP_FAILED;

	return mm;
}

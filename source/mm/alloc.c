
#include <stdlib.h>
#include <errno.h>

#include <video-clib/mm/alloc.h>
#include <video-clib/error.h>


void*
par_malloc(size_t size) {
	void* p = malloc(size);
	if(p == NULL)
		die("Not enaught memory");
	return p;
}

void*
par_realloc(void* ptr, size_t size) {
	void* _ptr = realloc(ptr, size);
	if(errno == ENOMEM)
		die("Cannot realloc memory block to the size %lu", size);
	return _ptr;
}

void
par_free(void* p) {
	free(p);
}

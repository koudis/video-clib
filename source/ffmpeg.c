
#include <dlfcn.h>

#include <config.h>

#include <video-clib/log.h>
#include <video-clib/macro.h>
#include <video-clib/assert.h>
#include <video-clib/ffmpeg.h>
#include <video-clib/ffmpeg/ffcommand.h>
#include <video-clib/mm/alloc.h>

char* ffmpeg_shlib_path WEAK = FFMPEG_SHLIB_PATH;



#ifdef FFMPEG_DLOPEN
int
ffmpeg(int argc, char** argv) {
	void* handle = dlopen(ffmpeg_shlib_path, RTLD_LAZY);
	if(!handle)
		die("Cannot locate ffmpeg.so");

	int (*_main)(int, char* argv[]);
	_main = dlsym(handle, "main");
	if(!_main)
		die("No main function in library");

	int ret = (*_main)(argc, argv);

	dlclose(handle);
	return ret;
}
#endif

#ifdef FFMPEG_EXEC
int
ffmpeg(struct ffmpeg_struct* pl) {
	die("ffmpeg_exec is not implemented yet");
}
#endif



static int
_ffmpeg_check_mem(struct ffmpeg_struct* pl);

int
ffmpegl_prepare(struct ffmpeg_struct* pl) {
	ASSERT(pl != NULL);

	_ffmpeg_check_mem(pl);
	int _s = ffcommand_export_array(pl->command, pl->mem, pl->mem_size);
	if(_s)
		LOG_AND_RETURN(L_CRIT, EF_CECA, "Cannot export command :(");

	return 0;
}

static int
_ffmpeg_check_mem(struct ffmpeg_struct* pl) {
	unsigned int size = ffcommand_size(pl->command);
	if(pl->mem_size < size) {
		xrealloc(pl->mem, size);
		pl->mem_size = size;
	}
	return 0;
}

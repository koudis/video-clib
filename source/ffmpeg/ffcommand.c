
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <syslog.h>

#include <video-clib/ffmpeg/ffcommand.h>

#include <video-clib/log.h>
#include <video-clib/macro.h>
#include <video-clib/mm/alloc.h>
#include <video-clib/mm/allocator.h>

#define FFCOMMAND_ALLOC_PAGE_SIZE 2048

#define FFCOMMAND_PART_MAX_DEFAULT 0
#define FFCOMMAND_PART_MIN_DEFAULT 0
#define FFCOMMAND_PART_STEP        3
#define FFCOMMAND_PART_MIN_STEP    2

#define _INPUT  0
#define _VIDEO  1
#define _AUDIO  2
#define _OUTPUT 3

#define _ORDER_BY_ORDER 1
#define _ORDER_BY_REAL_ORDER 0

COMPILE_ASSERT(ffcommand_min_step, FFCOMMAND_PART_STEP >= FFCOMMAND_PART_MIN_STEP);



static void ffcommand_parts_init(struct ffcommand_part** const part);
static void ffcommand_parts_destroy(struct ffcommand_part** const part);

// Comporator fo simclist
static int ffmpeg_list_comparator(const void* a, const void* b);

// Create netry fo linked list
static struct ffcommand_list_entry* ffcommand_create_entry(struct allocator* al,
		const char* const val, struct ffcommand* command, const int real_order);

// Just add entry to the part structure
static int ffcommand_add_to_part(struct ffcommand* const command, int sec,
		const char* const opt, const char* const val, const int);






int ffcommand_init(struct ffcommand* command) {
	if(command == NULL)
		return 1;

	struct allocator* allocator = xmalloc(sizeof(struct allocator));
	allocator_init(allocator, FFCOMMAND_ALLOC_PAGE_SIZE);

	void* _al2 = allocator_malloc(allocator, 4 * sizeof(struct ffcommand_part));
	*command = (struct ffcommand) {
			.allocator      = allocator,
			.al_state       = ALLOCATOR_PAGE_STATIC_INIT(0),
			.order_priority = _ORDER_BY_ORDER,
			.lock  = 0,
			.parts = {
					_al2,
					_al2 + sizeof(struct ffcommand_part),
	 				_al2 + 2*sizeof(struct ffcommand_part),
					_al2 + 3*sizeof(struct ffcommand_part)
			}
	};

	ffcommand_parts_init(command->parts);
	allocator_create_newpage(command->allocator);

	return 0;
}

void ffcommand_destroy(struct ffcommand* command) {
	ffcommand_parts_destroy(command->parts);
	allocator_destroy(command->allocator);
	free(command->allocator);
}



int ffcommand_lock(struct ffcommand* command) {
	if(command->lock)
		return 0;

	if(!ALLOCATOR_STATE_VALID(&command->al_state))
		allocator_state_save(command->allocator, &command->al_state);
	if(allocator_create_newpage(command->allocator))
		LOG_AND_RETURN(L_CRIT, 1,
			   	"Cannot create new page for 'lock' in ffcommand");

	allocator_page_lock(command->allocator);
	command->lock = 1;

	for(int i = 0; i < 4; i++)
		command->parts[i]->persist = list_size(&command->parts[i]->list);
	return 0;
}

int ffcommand_unlock(struct ffcommand* command, int hold) {
	ASSERT("UNDEFINE HOLD!" && ENUM_UNIQ(hold, FFORD_MASK));
	if(!command->lock)
		return 1;

	command->order_priority = _ORDER_BY_REAL_ORDER;
	for(int i = 0; i < 4; i++) {
		if(list_sort(&command->parts[i]->list, 1))
			LOG_AND_RETURN(L_CRIT, E_CRIT,
					"Cannot sort all sections by ORDER");
	}
	command->order_priority = _ORDER_BY_ORDER;

	for(int i = 0; i < 4; i++) {
		struct ffcommand_part* part = command->parts[i];
		unsigned int size = list_size(&part->list);
		if(size > part->persist)
			list_delete_range(&part->list, part->persist, size - 1);
	}

	switch(hold & FFPAGE_MASK) {
		case FFPAGE_HOLD:
			allocator_cleanpage(command->allocator);
			allocator_page_unlock(command->allocator);
			break;
		case FFPAGE_DELETE:
			allocator_page_unlock(command->allocator);
			if(allocator_delete_lastpage(command->allocator))
				LOG_AND_RETURN(L_CRIT, E_CRIT, "Cannot delete last page in command allocator");
			allocator_state_restore(command->allocator, &command->al_state);
			break;
	}
	command->lock = 0;
	return 0;
}



int ffcommand_export_string(struct ffcommand* command, char* al, size_t al_size) {
	list_t* list;
	size_t sum = 0;

	ASSERT(al != NULL);
	if(!al_size)
		return -1;

	int err = 0;
	int i;
	for(i = 0; i < 4; i++) {
		list = &command->parts[i]->list;
		if(list_sort(list, 1))
			die("Cannot sort");

		size_t length;
		struct ffcommand_list_entry* part;	

		list_iterator_start(list);
		while(list_iterator_hasnext(list)) {
			part = (struct ffcommand_list_entry*)list_iterator_next(list);
			ASSERT(part->value);
			if(!part->value)
				continue;

			length = strlen(part->value);	
			if(sum + length + 1 > al_size) {
				par_log(L_CRIT, "Not enaught memory.");
				err = -1;
				break;
			}

			strncpy(al + sum, " ", 1);
			strncpy(al + sum + 1, part->value, length);
	
			sum += length + 1;
		}
		list_iterator_stop(list);
		if(err)
			break;
	}
	if(err < 0)
		return err;
	if(sum + 1 > al_size)
		return -1;

	al[sum] = '\0';

	return (sum);
}

int ffcommand_export_array(struct ffcommand* command, char** al, size_t al_size) {
	list_t* list;
	size_t sum = 0;

	ASSERT(al != NULL || al_size > 0);
	if(!al_size)
		return -1;

	int k = 0, err = 0;
	int i;
	for(i = 0; i < 4; i++) {
		list = &command->parts[i]->list;
		if(list_sort(list, 1))
			die("Cannot sort");

		struct ffcommand_list_entry* part;

		list_iterator_start(list);
		while(list_iterator_hasnext(list)) {
			part = (struct ffcommand_list_entry*)list_iterator_next(list);
			ASSERT(part->value);
			if(!part->value)
				continue;
	
			if(sum + 1 > al_size) {
				par_log(L_CRIT, "Not enaught memory.");
				err = -1;
				break;
			}

			al[k] = part->value;

			sum += 1;
			k   += 1;
		}
		list_iterator_stop(list);
		if(err)
			break;
	}
	if(err < 0)
		return err;
	if(sum + 1 > al_size)
		return -1;

	al[sum] = 0;

	return (sum);
}



int ffcommand_add_video(struct ffcommand* command,
		const char* const opt, const char* const val, int order) {
	return ffcommand_add_to_part(command, _VIDEO,
		   	opt, val, order);
}

int ffcommand_add_audio(struct ffcommand* command,
		const char* const opt, const char* const val, int order) {
	return ffcommand_add_to_part(command, _AUDIO,
			opt, val, order);
}

int ffcommand_add_input(struct ffcommand* command,
		const char* const opt, const char* const val, int order) {
	return ffcommand_add_to_part(command, _INPUT,
			opt, val, order);
}

int ffcommand_add_output(struct ffcommand* command,
		const char* const opt, const char* const val, int order) {
	return ffcommand_add_to_part(command, _OUTPUT,
			opt, val, order);
}






// Local functions

static int ffmpeg_list_comparator(const void* a, const void* b) {
	struct ffcommand_list_entry* const e_a = (struct ffcommand_list_entry*)a;	
	struct ffcommand_list_entry* const e_b = (struct ffcommand_list_entry*)b;

	ASSERT(e_a->command != NULL);

	if(e_a->command->order_priority) {
		if(e_a->order < e_b->order)
			return 1;
		if(e_a->order > e_b->order)
			return -1;
		return 0;
	}

	if(e_a->real_order < e_b->real_order)
		return 1;
	if(e_a->real_order > e_b->real_order)
		return -1;
	return 0;	
}

static void ffcommand_parts_init(struct ffcommand_part** const part) {
	for(int i = 0; i < 4; i++) {
		*part[i] = (struct ffcommand_part) {
			.max     = FFCOMMAND_PART_MAX_DEFAULT,
			.min     = FFCOMMAND_PART_MIN_DEFAULT,
			.persist = 0,
		};
		list_init(&(part[i]->list));
    	list_attributes_comparator(&part[i]->list, ffmpeg_list_comparator);
	}
}

static void ffcommand_parts_destroy(struct ffcommand_part** const part) {
	for(int i = 0; i < 4; i++)
		list_destroy(&part[i]->list);
}

static struct ffcommand_list_entry* ffcommand_create_entry(struct allocator* al,
		const char* const val, struct ffcommand* command, const int real_order) {

	struct ffcommand_list_entry* entry = allocator_malloc(al, sizeof(struct ffcommand_list_entry));
	if(entry == NULL)
		die("Cannot allocate ffcommand entry");

	*entry = (struct ffcommand_list_entry) {
		.value      = NULL,
		.command    = command,
		.order      = 0,
		.real_order = real_order,
	};

	if(!val) {
		entry->value = NULL;
	} else {
		entry->value = allocator_malloc(al, strlen(val) + 1);
		strcpy(entry->value, val);
	}

	return entry;
}

inline static int ffcommand_add_to_part(struct ffcommand* const command, int sec,
		const char* const opt, const char* const val, const int order) {

	ASSERT("UNDEFINED ORDER!" && ENUM_UNIQ(order, FFORD_MASK));
	ASSERT("opt arg is null!" && opt != NULL);

	struct ffcommand_part* const part = command->parts[sec];
	struct allocator*      const al   = command->allocator;

	int real_order = list_size(&part->list);
	struct ffcommand_list_entry* opt_entry = ffcommand_create_entry(al, opt, command, real_order);
	struct ffcommand_list_entry* val_entry = NULL;
   	if(val != NULL)
		val_entry = ffcommand_create_entry(al, val, command, real_order + 1);

	list_append(&part->list, opt_entry);
	if(val)
		list_append(&part->list, val_entry);

	int opt_order, val_order;
	switch(order) {
		case FFORD_HEAD:
			ASSERT(part->min - FFCOMMAND_PART_STEP < 0);
			part->min = part->min - FFCOMMAND_PART_STEP;
			opt_order = part->min;
			break;
		case FFORD_TAIL:
		case FFORD_CURRENT:
			ASSERT(part->max + FFCOMMAND_PART_STEP > 0);
			part->max = part->max + FFCOMMAND_PART_STEP;
			opt_order = part->max;
			break;
		default:
			par_log(LOG_CRIT, "Undefined order");
			return 1;
	}

	opt_entry->order = opt_order;
	if(val) {
		val_order        = opt_order + 1;
		val_entry->order = val_order;
	}

	return 0;
}

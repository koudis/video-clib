
#include "video-clib/ffmpeg/ffcommand_list.h"
#include "video-clib/ffmpeg/ffcommand.h"
#include <video-clib/log.h>


void ffcommand_list_threads(struct ffcommand* command) {
	ffcommand_add_output(command, "-threads", "4", FFORD_TAIL);
}

int ffcommand_list_astring(struct ffcommand* command, char* mem, size_t size,
		const char* const input_file, const char* const output_file) {

	if(ffcommand_lock(command))
		LOG_AND_RETURN(L_CRIT, E_CRIT, "ffcommand_lock failed");
	if(ffcommand_add_input(command, "-i", input_file, FFORD_TAIL))
		LOG_AND_RETURN(L_CRIT, E_CRIT, "ffcomand_add_input failed");
	if(ffcommand_add_output(command, output_file, NULL, FFORD_TAIL))
		LOG_AND_RETURN(L_CRIT, E_CRIT, "ffcommand_add_output failed");
	if(ffcommand_export_string(command, mem, size))
		LOG_AND_RETURN(L_CRIT, E_CRIT, "ffcommand_export_string failed");
	if(ffcommand_unlock(command, FFPAGE_HOLD))
		LOG_AND_RETURN(L_CRIT, E_CRIT, "ffcommand_unlock");

	return 0;
}

int ffcommand_list_set_iofiles(struct ffcommand* command,
		const char* const input_file, const char* const output_file) {

	int _s;
	_s = ffcommand_add_input(command, "-i", input_file, FFORD_TAIL);
	if(_s)
		LOG_AND_RETURN(L_CRIT, _s, "ffcomand_add_input failed");

	ffcommand_add_output(command, output_file, NULL, FFORD_TAIL);
	if(_s)
		LOG_AND_RETURN(L_CRIT, _s, "ffcommand_add_output failed");

	return 0;
}

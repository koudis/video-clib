
#include <video-clib/error.h>
#include <video-clib/log.h>



void _die(const char* message, ...) {
	va_list va_l;
	va_start(va_l, message);

	par_vlog(L_CRIT, message, va_l);
	abort();
}
